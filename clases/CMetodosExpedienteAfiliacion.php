<?php
include_once ("global.php");
include_once ("CLogImpresion.php");
include_once ("Capirestsolicitudafiliacion.php");
define('RUTA_LOGX',					'/sysx/progs/afore/log/metodosSolicitudAfiliacion');
define("RUTA_SALIDA","/sysx/progs/web/salida/");

class CMetodosExpedienteAfiliacion
{
	function __construct()
	{
		date_default_timezone_set('America/Mazatlan');
	}
	function __destruct()
	{
	}

	var $cnxDb;
	var $arrError;

	public function grabarLogx($cadLogx)
	{
		$cIpCliente = $this->getRealIP();
		$rutaLog =  RUTA_LOGX .  '-' . date("Y-m-d") . ".log";
		$cad = date("Y-m|H:i:s|") . getmypid() . "|" . $cIpCliente . "| " . $cadLogx . "\n";
		$file = fopen($rutaLog, "a");
		if( $file )
		{
			fwrite($file, $cad);
		}
		fclose($file);
	}

    public function getRealIP()
    {
        if (!empty($_SERVER['HTTP_CLIENT_IP']))
            return $_SERVER['HTTP_CLIENT_IP'];

        if (!empty($_SERVER['HTTP_X_FORWARDED_FOR']))
            return $_SERVER['HTTP_X_FORWARDED_FOR'];

        return $_SERVER['REMOTE_ADDR'];
    }

	public function leerXmlServ($archivo, $idElem)
	{
		$res = "";
		if( file_exists($archivo) )
		{
			$datosXml = simplexml_load_file($archivo, "SimpleXMLElement",LIBXML_NOCDATA);
			if($datosXml)
			{
				echo "leyo xml 0000 <br>";
				print_r($datosXml);

				foreach($datosXml->Servidor as $elem)
				{
					echo "<br> " . $idElem .  ' Vs ' .  $elem->key . "<br>";
					print_r($elem);

					if( $elem->id == $idElem )
						$res = $elem;
				}
			}
			else
			{
				$this->grabarLogx("formato XML invalido");
				$res = false;
			}
		}
		else
		{
			$res = "El archivo " . $archivo . " no existe";
			$this->grabarLogx($res);
			$res = false;
		}
		return $res;
	}

	public function obtenerConexionBdPostgres($DireccionIp, $BaseDato, $UsrBaseDato, $PasswdBaseDato)
	{
		global $cnxDb;
		if( pg_connection_status($cnxDb) != PGSQL_CONNECTION_OK )
		{
			$cadConexion = "host=" . $DireccionIp . " dbname=" . $BaseDato . " user=" . $UsrBaseDato . " password=" . $PasswdBaseDato;
			$cnxDb = pg_pconnect($cadConexion);
			if( $cnxDb )
				$this->grabarLogx('[obtenerConexionBdPostgres] Conexion establecida');
		}

		return $cnxDb;
	}

	public function obtenerXML($array)
	{
		$xml = "";
		$xml = "<map>";
		foreach($array as $key => $value)
		{
			$mykey = $key;
			$myvalue= $value;
			$xml .= "<entry><key>".trim($mykey)."</key>"."<value>".trim($myvalue)."</value></entry>";
		}
		$xml .= "</map>";
		return $xml;
	}

	public function obtenerCurpPromotor($iNumeroEmpleado)
	{
		global $cnxDb;
		$objAPI = new Capirestsolicitudafiliacion();
		$arrDatos['respuesta'] = 0;
		$arrDatos['curpPromotor'] = "";
		$arrDatos['descripcion'] = "";

		$arrAPI = array('iNumeroEmpleado' => $iNumeroEmpleado);

		try
		{

			$this->grabarLogx("Inicio de ejecucion de API Rest");

			$resultAPI = $objAPI->consumirApi('obtenerCurpPromotor',$arrAPI);

			$this->grabarLogx("Respuesta Api: ".$resultAPI);

			$this->grabarLogx("Fin de ejecucion de API Rest");

			if($resultAPI)
			{
				$resultAPI = json_decode($resultAPI, true);

				if($resultAPI['estatus'] == 1)
				{
					//$arrDatos['fecha'] = $resulSet['sfechaalta'];
					foreach($resultAPI['registros'] as $reg => $value)
					{
						$arrDatos['curpPromotor'] = $value['curp'];
					}
					 $sCurpPromotor =$arrDatos['curpPromotor'];

					if($sCurpPromotor != "" )
					{
						$arrDatos['respuesta'] = 1;
						$arrDatos['descripcion'] = "Curp Valido";

					}

				}
				else
				{
					// Si existe un error en la consulta mostrará el siguiente mensaje
					$arrDatos['respuesta'] = -1;
					$arrDatos['descripcion'] = "Se presento un problema al ejecutar ĺa consulta";

					throw new Exception("CMetodosExpedienteAfiliacion.php\obtenerCurpPromotor"."\tError al ejecutar la consulta \t"." | " . $arrError[0] . '-' . $arrError[1] . '-' . $arrError[2] );
				}
				//cierra la conexion a base de datos

			}
			else
			{
				$arrDatos['respuesta'] = -1;
				$arrDatos['descripcion'] = "No abrio Conexion a PostGreSQL";
			}
		}
		catch (Exception $e)
		{
			//Cacha la execcion por la que fallo la ejecucion del query y lo manda como parametro  para escribir la descriccion del error.
		    $mensaje= 'Excepcion: ' . $e->getMessage() . ' Linea: ' . $e->getLine() .    '  Codigo: ' .  $e->getCode();
		}

		return $arrDatos;
	}

	//METODO PARA CONVERTIR EL PDF A IMAGEN TIF
	function convertirPDFaImagen($iFolio, $sCurp, $iClaveOperacion, $sArchivoPFD, $iDentificadorProceso, $iSiefore)
	{
		$sNombreImagenOut = "";
		$sNombreImagenAuxOut = "";
		$sFechaActual = date("Ymd");
		$sNomenclatura = "";
		//Se crea objeto Imagick
		$im = new Imagick();

		$arrResp = array();
		$arrResp["estado"] = -1;
		$arrResp["archivo"] = "";
		$arrResp["rutaimagen"] = "";

		//Se asigna la resolución de la imagen
		$im->setResolution(300,300);

		//Se lee la ruta donde se encuentra el PDF
		$im->readImage($sArchivoPFD);

		$im->setImageFormat('tif');
		$im->setImageBackgroundColor("white");
		$im->setImageUnits(imagick::RESOLUTION_PIXELSPERINCH);
		$im->transformImageColorspace(Imagick::COLORSPACE_GRAY);

		switch($iDentificadorProceso)
		{
			//SOLICITUD DE CONSTANCIA
			case 1:
				$im->resizeImage(2550,3300,Imagick::FILTER_BOX,1);
				$sNomenclatura = "SCTR";
				$sRutaSalidaFinal = RUTA_SALIDA.'constanciasafiliacion/';
				break;
			//IMPRESION DE CONSTANCIA
			case 2:
				$im->resizeImage(3300,2550,Imagick::FILTER_BOX,1);
				$sNomenclatura = "CTR0";
				$sRutaSalidaFinal = RUTA_SALIDA.'constanciasafiliacion/';
				break;
			//SOLICITUD AFILIACION REGISTRO
			case 3:
				$im->resizeImage(2544,4019,Imagick::FILTER_BOX,1);
				$sNomenclatura = "SR00";
				$sRutaSalidaFinal = RUTA_SALIDA.'solicitudafiliacion/';
				break;
			//SOLICITUD AFILIACION TRASPASO
			case 4:
				$im->resizeImage(2544,4019,Imagick::FILTER_BOX,1);
				$sNomenclatura = "ST00";
				$sRutaSalidaFinal = RUTA_SALIDA.'solicitudafiliacion/';
				break;
			//CONTRATO DE ADM. DE FONDOS
			case 5:
				$im->resizeImage(2550,3300,Imagick::FILTER_BOX,1);
				$sNomenclatura = "CA00";
				$sRutaSalidaFinal = RUTA_SALIDA.'contratosolicitud/';
				break;
			//DOCTO RENDIMIENTO REGISTRO
			case 6:
				$im->resizeImage(2550,3300,Imagick::FILTER_BOX,1);
				$sRutaSalidaFinal = RUTA_SALIDA.'doctorn/';
				break;
			//DOCTO RENDIMIENTO TRASPASO
			case 7:
				$im->resizeImage(3300,2550,Imagick::FILTER_BOX,1);
				$sRutaSalidaFinal = RUTA_SALIDA.'doctorn/';
				break;
			//EN CASO DE RECIBIR UN VALOR DIFERENTE

			// //SOLICITUD AFILIACION TRASPASO
			// case 8:
			// 	$im->resizeImage(2544,4019,Imagick::FILTER_BOX,1);
			// 	$sNomenclatura = "ST02";
			// 	$sRutaSalidaFinal = RUTA_SALIDA.'solicitudafiliacion/';
			// 	break;
			// //SOLICITUD AFILIACION TRASPASO
			// case 9:
			// 	$im->resizeImage(2544,4019,Imagick::FILTER_BOX,1);
			// 	$sNomenclatura = "ST03";
			// 	$sRutaSalidaFinal = RUTA_SALIDA.'solicitudafiliacion/';
			// 	break;
			// //SOLICITUD AFILIACION TRASPASO
			// case 10:
			// 	$im->resizeImage(2544,4019,Imagick::FILTER_BOX,1);
			// 	$sNomenclatura = "ST04";
			// 	$sRutaSalidaFinal = RUTA_SALIDA.'solicitudafiliacion/';
			// 	break;
			// //SOLICITUD AFILIACION REGISTRO
			// case 11:
			// 	$im->resizeImage(2544,4019,Imagick::FILTER_BOX,1);
			// 	$sNomenclatura = "SR02";
			// 	$sRutaSalidaFinal = RUTA_SALIDA.'solicitudafiliacion/';
			// 	break;
			// //SOLICITUD AFILIACION REGISTRO
			// case 12:
			// 	$im->resizeImage(2544,4019,Imagick::FILTER_BOX,1);
			// 	$sNomenclatura = "SR03";
			// 	$sRutaSalidaFinal = RUTA_SALIDA.'solicitudafiliacion/';
			// 	break;
			// //SOLICITUD AFILIACION REGISTRO
			// case 13:
			// 	$im->resizeImage(2544,4019,Imagick::FILTER_BOX,1);
			// 	$sNomenclatura = "SR04";
			// 	$sRutaSalidaFinal = RUTA_SALIDA.'solicitudafiliacion/';
			// 	break;
			default:
				break;

		}

		switch($iSiefore)
		{
			//SIEFORE 1
			case 1:
				$sNomenclatura = "RN01";
				break;
			//SIEFORE 2
			case 2:
				$sNomenclatura = "RN02";
				break;
			//SIEFORE 3
			case 3:
				$sNomenclatura = "RN03";
				break;
			//SIEFORE 4
			case 4:
				$sNomenclatura = "RN04";
				break;
			//EN CASO DE RECIBIR UN 0
			default:
				break;
		}

		$im->setImageType(Imagick::IMGTYPE_BILEVEL);
		$im->setImageDepth(1);
		$im->setImageChannelDepth(Imagick::CHANNEL_ALL, 1);
		$im->setImageCompression(Imagick::COMPRESSION_GROUP4);

		//Se aplica la calidad de 100% a la imagen
		$im->setCompressionQuality(100);

		//Se guarda la imagen
		$sNombreImagenOut = sprintf("%d_%018s%06d%s%s.tif",$iFolio, $sCurp, $iClaveOperacion, $sFechaActual, $sNomenclatura );
		if($im->writeImage($sRutaSalidaFinal.$sNombreImagenOut))
		{
			self::grabarLogx("SE GENERO IMAGEN CON EXITO");
			$arrResp["estado"] = OK___;
			$arrResp["archivo"] = $sNombreImagenOut;
			$arrResp["rutaimagen"] = $sRutaSalidaFinal.$sNombreImagenOut;
		}

		return $arrResp;
	}

	//METODO PARA CONVERTIR LA IMAGEN DE LA HUELLA DE TIF A JPG
	function convertirHuella($sRutaHuellaTrabajador, $iFolio, $iTipoSolicitud)
	{
		$sNomenclatura = "";
		//SE CREA OBJETO IMAGICK
		$im = new Imagick();

		$arrResp = array();
		$arrResp["estado"] = -1;

		//SE LEE LA IMAGEN DE LA HUELLA
		$im->readImage($sRutaHuellaTrabajador);

		//SE ASIGNA EL FORMATO JPG A LA NUEVA IMAGEN QUE SE VA A GENERAR
		$im->setImageFormat('jpg');

		if($iTipoSolicitud == 26)
		{
			$sNomenclatura = "SR00";
		}
		else
		{
			$sNomenclatura = "ST00";
		}

		//SE GUARDA IMAGEN AHORA CON FORMATO JPG
		if($im->writeImage('/sysx/progs/web/entrada/huellas/'.$sNomenclatura.'_'.$iFolio.'_HTRAB.JPG'))
		{
			self::grabarLogx("SE GENERO IMAGEN JPG CON EXITO");
			$arrResp["estado"] = OK___;
		}

		return $arrResp;
	}

	//METODO PARA UNIR LA IMAGEN DEL DOCUMENTO CON LAS FIRMAS
	function unirImagenes($sImagenSolicitud, $sRutaImagenDocumento, $sRutaImagenFirmaTrabajador, $sRutaImagenFirmaPromotor)
	{
		//Se crean tres objetos
		$imagenDocumento = new Imagick($sRutaImagenDocumento);
		$imagenFirmaPromotor = new Imagick($sRutaImagenFirmaPromotor);
		$imagenFirmaTrabajador = new Imagick($sRutaImagenFirmaTrabajador);

		$arrResp = array();
		$arrResp["estado"] = -1;
		$sImagenFinal = $sImagenSolicitud;

		//Se modifica el tamaño de la firma
		$imagenFirmaPromotor->resizeImage(825, 283, Imagick::FILTER_BOX, 1);
		$imagenFirmaTrabajador->resizeImage(825, 283, Imagick::FILTER_BOX, 1);

		//La segunda imagen se pone arriba de la primera imagen
		$imagenDocumento->compositeImage($imagenFirmaPromotor, Imagick::COMPOSITE_BUMPMAP, 270, 2250);
		//La tercera imagen se pone arriba de la primera imagen
		$imagenDocumento->compositeImage($imagenFirmaTrabajador, Imagick::COMPOSITE_BUMPMAP, 1540, 2250);

		if($imagenDocumento->writeImage($sRutaImagenDocumento))
		{
			self::grabarLogx("SE UNIERON IMAGENES CON EXITO");
			$arrResp["estado"] = OK___;
		}

		return $arrResp;
	}

	//METODO PARA UNIR LA IMAGEN DEL DOCUMENTO CON LA HUELLA DEL TRABAJADOR Y LA FIRMA DEL PROMOTOR
	function unirImagenesHuella($sImagenSolicitud, $sRutaImagenDocumento, $sRutaHuellaTrabajador, $sRutaImagenFirmaPromotor)
	{
		//Se crean tres objetos
		$imagenDocumento = new Imagick($sRutaImagenDocumento);
		$imagenFirmaPromotor = new Imagick($sRutaImagenFirmaPromotor);
		$imagenHuellaTrabajador = new Imagick($sRutaHuellaTrabajador);

		$arrResp = array();
		$arrResp["estado"] = -1;

		//Se modifica el tamaño de las imagenes
		$imagenFirmaPromotor->resizeImage(825, 283, Imagick::FILTER_BOX, 1);
		$imagenHuellaTrabajador->resizeImage(175, 229, Imagick::FILTER_BOX, 1);

		//La segunda imagen se pone arriba de la primera imagen
		$imagenDocumento->compositeImage($imagenFirmaPromotor, Imagick::COMPOSITE_BUMPMAP, 270, 2250);
		//La tercera imagen se pone arriba de la primera imagen
		$imagenDocumento->compositeImage($imagenHuellaTrabajador, Imagick::COMPOSITE_BUMPMAP, 1800, 2280);

		if($imagenDocumento->writeImage($sRutaImagenDocumento))
		{
			self::grabarLogx("SE UNIERON IMAGENES CON EXITO");
			$arrResp["estado"] = OK___;
		}

		return $arrResp;
	}

	//METODO PARA PUBLICAR IMAGENES EN EL SERVIDOR INTERMEDIO
	function publicarImagen($sArchivo, $iDentificadorProceso)
	{
		switch($iDentificadorProceso)
		{
			//SOLICITUD DE CONSTANCIA
			case 1:
				$sRutaSalidaFinal = RUTA_SALIDA.'constanciasafiliacion/';
				break;
			//IMPRESION DE CONSTANCIA
			case 2:
				$sRutaSalidaFinal = RUTA_SALIDA.'constanciasafiliacion/';
				break;
			//SOLICITUD AFILIACION REGISTRO
			case 3:
				$sRutaSalidaFinal = RUTA_SALIDA.'solicitudafiliacion/';
				break;
			//SOLICITUD AFILIACION TRASPASO
			case 4:
				$sRutaSalidaFinal = RUTA_SALIDA.'solicitudafiliacion/';
				break;
			//CONTRATO DE ADM. DE FONDOS
			case 5:
				$sRutaSalidaFinal = RUTA_SALIDA.'contratosolicitud/';
				break;
			//DOCTO RENDIMIENTO REGISTRO
			case 6:
				$sRutaSalidaFinal = RUTA_SALIDA.'doctorn/';
				break;
			//DOCTO RENDIMIENTO TRASPASO
			case 7:
				$sRutaSalidaFinal = RUTA_SALIDA.'doctorn/';
				break;
			//EN CASO DE RECIBIR UN VALOR DIFERENTE
			default:
				break;
		}

		$nombreImagen = $sRutaSalidaFinal.$sArchivo;
		$cadena = "";
		$arrREsp = array();
		$arrREsp["estado"] = -1;
		$iRet = -1;

		$cadena = '/usr/local/bin/wput -B -t 10 -u '.$nombreImagen.' ftp://'.USUARIO_PUBLICA_FTP.':'.PASS_PUBLICA_FTP.'@'.IP_PUBLICACION_IMAGENES.'/sysx/smbxtempol/'.$sArchivo;

		$iRet = self::my_shell_exec($cadena);
		self::grabarLogx(" iRet: ".$iRet);

		if($iRet == 0)
		{
			$arrREsp["estado"] = 1;
			self::grabarLogx("PUBLICO CON EXITO");
		}

		return $arrREsp;
	}

	function my_shell_exec($cmd, &$stdout=null, &$stderr=null) {

		$proc = proc_open($cmd,[
			1 => ['pipe','w'],
			2 => ['pipe','w'],
		],$pipes);
		$stdout = stream_get_contents($pipes[1]);
		fclose($pipes[1]);
		$stderr = stream_get_contents($pipes[2]);
		fclose($pipes[2]);
		return proc_close($proc);
	}

	function actualizarFirmaPublicacion($iFolio,$iOpcion)
	{
		global $cnxDb;
		$objAPI = new Capirestsolicitudafiliacion();
		$arrResp = array();
		$arrResp["estado"] = -1;

		$arrAPI = array(
				'iFolio' => $iFolio,
				'iOpcion' => $iOpcion
			);

		try
		{
			$this->grabarLogx("Inicio de ejecucion de API Rest");

			$resultAPI = $objAPI->consumirApi('actualizarFirmaPublicacion',$arrAPI);

			$this->grabarLogx("Respuesta Api: ".$resultAPI);

			$this->grabarLogx("Fin de ejecucion de API Rest");

			if($resultAPI)
			{
				$resultAPI = json_decode($resultAPI, true);

				if($resultAPI['estatus'] == 1)
				{
					$arrResp["estado"] = 1;
					self::grabarLogx('[CMetodosExpedienteAfiliacion::actualizarfirmapublicacion]Estado:'.$arrResp["estado"]);
					self::grabarLogx("SE ACTUALIZO FIRMA CORRECTAMENTE");
				}
			}
		}
		catch (Exception $e)
		{
			//Cacha la execcion por la que fallo la ejecucion del query y lo manda como parametro  para escribir la descriccion del error.
			 $mensaje= 'Excepcion: ' . $e->getMessage() . ' Linea: ' . $e->getLine() .    '  Codigo: ' .  $e->getCode();
			 self::grabarLogx('[CMetodosExpedienteAfiliacion::actualizarfirmapublicacion]'. $mensaje);
		}

		return $arrResp;
	}

	//ACTUALIZA O INSERTA EN LA TABLA "ctrlimagenesportransmitir" Y POSTERIORMENTE SE GENERE Y TRANSMITA EL PDF/IMAGEN AL SERVIDOR BULL DOZER.
	//SUSTITUYE A LA FUNCION actualizarFirmaPublicacion para que ya no se registre en la tabla "stmppublicarimagen".
	function ctrlImagenesPorTransmitir($iOpcion,$iFolio,$cNombreArchivo)
	{
		self::grabarLogx("Entro a ctrlImagenesPorTransmitir.");
		
		try
		{
			$arrResp = array();
			$arrResp['codigorespuesta'] =0;
			$arrResp['descripcion'] ='';
			$arrResp['estado'] = -1;
			$i = 0;
			
			//Se abre conexion a BD (aforeglobal).
			$cnxDb =  new PDO( "pgsql:host=".IPSERVIDOR.";port=5432;dbname=".BASEDEDATOS, USUARIO, PASSWORD);
			
			//Verificamos si la conexion se abrio exitosamente.
			if($cnxDb)
			{
				self::grabarLogx("Se abrio conexion a BD (aforeglobal).");
				
				$cSql = "SELECT tfolio FROM fnctrlimagenesportransmitir($iOpcion, $iFolio, '', 0, '$cNombreArchivo');";
				
				self::grabarLogx("Ejecutada query [ctrlImagenesPorTransmitir]: $cSql");
				
				$resulSet = $cnxDb->query($cSql);
				if($resulSet)
				{
					$arrResp['estado'] = 1;
					
					foreach($resulSet as $reg)
					{
						$arrResp['tfolio'] = $reg['tfolio'];
						$i++;
					}
					if($i > 0)
					{
						$arrResp['codigorespuesta'] = 1;
						$arrResp['descripcion'] = "Imagen [$cNombreArchivo] registrada en BD para trasmitir correctamente.";
						self::grabarLogx($arrResp['descripcion']);
					}
					else
					{
						$arrResp['codigorespuesta'] = 0;
						$arrResp['descripcion'] = "No se registro en BD la imagen para transimitir $cNombreArchivo: $cSql";
						self::grabarLogx($arrResp['descripcion']);
					}
				}
				else
				{
					$arrResp['codigorespuesta'] = 0;
					$arrResp['descripcion'] = "Error al ejecutar query [ctrlImagenesPorTransmitir] (verificar que el folio exista en la tabla solconstancia): $cSql";
					self::grabarLogx($arrResp['descripcion']);
				}
			}
			else
			{
				//Regresa el error al usuario
				$arrResp['codigorespuesta'] = 0;
				$arrResp['descripcion'] = "Error al abrir conexion a BD (aforeglobal) en query [ctrlImagenesPorTransmitir]: $cSql";
				self::grabarLogx($arrResp['descripcion']);
			}
		}
		catch (Exception $e)
		{
			$mensaje= 'Excepcion [ctrlImagenesPorTransmitir]: ' . $e->getMessage() . ' Linea: ' . $e->getLine() .    '  Codigo: ' .  $e->getCode();
			$arrResp['descripcion'] = $mensaje;
			self::grabarLogx($arrResp['descripcion']);
		}
		
		$cnxDb = null;
		self::grabarLogx("Se cierra conexion a BD (aforeglobal).");
		
		return $arrResp;
	}
	
	function obtenerclaveoperacion($iFolio, $iTabla)
	{
		global $cnxDb;
		$objAPI = new Capirestsolicitudafiliacion();
		$arrResp = array();
		$arrResp["estado"] = -1;

		$arrAPI = array(
			'iFolio' => $iFolio,
			'iTabla' => $iTabla
		);

		try
		{
			$this->grabarLogx("Inicio de ejecucion de API Rest");

			$resultAPI = $objAPI->consumirApi('obtenerclaveoperacion',$arrAPI);

			$this->grabarLogx("Respuesta Api: ".$resultAPI);

			$this->grabarLogx("Fin de ejecucion de API Rest");

			if($resultAPI)
			{
				$resultAPI = json_decode($resultAPI, true);

				if($resultAPI['estatus'] == 1)
				{
					foreach ($resultAPI['registros'] as $key => $value)
					{
						$arrResp['respuesta'] = $value['fnobtenerdoctosdigitalizacion'];
						$arrResp["estado"] = 1;
					}
					self::grabarLogx('[CMetodosExpedienteAfiliacion::obtenerclaveoperacion]Estado:'.$arrResp["estado"]);
				}
			}
		}
		catch (Exception $e)
		{
			//Cacha la exepcion por la que fallo la ejecucion del query y lo manda como parametro  para escribir la descripcion del error.
			 $mensaje= 'Excepcion: ' . $e->getMessage() . ' Linea: ' . $e->getLine() .    '  Codigo: ' .  $e->getCode();
			 self::grabarLogx('[CMetodosExpedienteAfiliacion::obtenerclaveoperacion]'. $mensaje);
		}

		return $arrResp;
	}

}

?>
