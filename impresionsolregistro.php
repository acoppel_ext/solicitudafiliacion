<?php
include("../fpdf/code39.php");
include("../fpdi/fpdi.php");
include_once ("clases/global.php");
include_once ("clases/CMetodosExpedienteAfiliacion.php");
include_once ("clases/CLogImpresion.php");
include_once ("clases/Capirestconstanciaafiliacion.php");

define("APP_DWSQ", "/sysx/progs/gsoap/wshuellas4mas1Afore/nbis/bin/dwsq");
define("RUTA_SALIDA_HLLA", "/sysx/progs/web/salida/solicitudafiliacion/");

date_default_timezone_set('America/Mazatlan');

$cnxBd = null;
$iContinuar = 0;
$iFirma = -1;
$hoja = 0;

$cTelefono1 = '';
$cTelefono2 = '';
$cTel_ref1 = '';
$cTel_ref2 = '';
$curpTrabajador = '';

$excepcion 			= 0;
$tiposolicitante 	= 0;

$arrErr = array();
$arrResp = array();

$cnxBd =  new PDO( "pgsql:host=".IPSERVIDOR.";port=5432;dbname=".BASEDEDATOS, USUARIO, PASSWORD);
if($cnxBd)
{

	if(isset($_GET['foliosolicitud']) && isset($_GET['firma']) )
	{
		$iFolio = $_GET['foliosolicitud'];
		$iFirma = $_GET['firma'];
		$hoja 	= $_GET['hoja'];

					$sSql = "";
					$sSql = "SELECT
								CASE substr(a.motivo,1,2)
									WHEN '00' THEN  substr(a.motivo,4,41)
									ELSE b.descripcion
								END AS motivo
							FROM colmotivotraspaso a
							INNER JOIN colcatmotivotraspaso b
							ON(b.id = substr(a.motivo,1,2)::INTEGER OR substr(a.motivo,1,2)::INTEGER = 0)
							AND a.folio = ".$iFolio." limit 1";
					$resulSet = $cnxBd->query($sSql);
					if($resulSet)
					{
						$cMotivoRegTras = 0;
						$cMotivoRegTras =  utf8_decode(utf8_encode($resulSet->fetchColumn()));
					}

					$sSql = "";
					$sSql = "SELECT COALESCE('('||lada ||')'|| telefono,'') AS telefonotrab FROM coltelefonos WHERE tipocontacto = 159  AND folio =".$iFolio;
					$resulSet = $cnxBd->query($sSql);
					if($resulSet)
					{
						$cTelTrabajo = 0;
						$cTelTrabajo =  $resulSet->fetchColumn();
					}
					$iContinuar = 1;

		if($iContinuar > 0)
		{
			$sSql = "";
			$sSql = "SELECT folio,fechafirma,fechavigencia,fechaimpresion,TRIM(nombre)AS nombre,TRIM(paterno)AS paterno,TRIM(materno)AS materno,nss,curp,rfc,fechanacimiento,
							genero,ocupacion,actgironegocio,nacionalidad,nivelestudios,calle,colonia,ciudad,delegacionmunicipio,
							entidadfederativa,pais,email,numeroexterior,numerointerior,codigopostal,telefono1,tipotelefono1,
							telefono2,tipotelefono2,calletrab,coloniatrab,ciudadtrab,delegacionmunicipiotrab,entidadfederativatrab,
							paistrab,numeroexteriortrab,numerointeriortrab,codigopostaltrab,telefono1trab,tipotelefono1trab,
							TRIM(nombresref1)AS nombresref1,TRIM(paternoref1)AS paternoref1,TRIM(maternoref1)AS maternoref1,curpref1,telref1,parentescoref1,nombresref2,paternoref2,
							maternoref2,curpref2,telref2,parentescoref2,nombresprom,paternoprom,maternoprom,curpprom,
							folioconst,cons_implica,edo_cuenta,afore_cedente,numregistro,nombresbenf1,paternobenf1,
							maternobenf1,curpbenf1,porcentajebenef1,parentescobenef1,nombresbenf2,paternobenf2,maternobenf2,
							curpbenf2,porcentajebenef2,parentescobenef2,nombresbenf3,paternobenf3,maternobenf3,curpbenf3,
							porcentajebenef3,parentescobenef3,nombresbenf4,paternobenf4,maternobenf4,curpbenf4,porcentajebenef4,
							parentescobenef4,nombresbenf5,paternobenf5,maternobenf5,curpbenf5,porcentajebenef5,parentescobenef5,
							conexion FROM colgenerasolicitudregtrasp(".$iFolio.");";
			$resulSet = $cnxBd->query($sSql);
			if($resulSet)
			{
				foreach($resulSet as $reg)
				{


					$arrDatosSolicitud  = array(	'folio' => $iFolio,
									'fechafirma' => $reg['fechafirma'],
									'fechavigencia' => $reg['fechavigencia'],
									'fechaimpresion' => $reg['fechaimpresion'],
									'nombre' => strpos(strtoupper($reg['nombre']), 'Ñ')===false ? $reg['nombre'] : utf8_decode($reg['nombre']),
									'paterno' => strpos(strtoupper($reg['paterno']), 'Ñ')===false ? $reg['paterno'] : utf8_decode($reg['paterno']),
									'materno' => strpos(strtoupper($reg['materno']), 'Ñ')===false ? $reg['materno'] : utf8_decode($reg['materno']),
									'nss' => utf8_decode($reg['nss']),
									'curp' => utf8_decode($reg['curp']),
									'rfc' => utf8_decode($reg['rfc']),
									'fechanacimiento' => $reg['fechanacimiento'],
									'genero' => utf8_decode($reg['genero']),
									'ocupacion' => utf8_decode(utf8_encode($reg['ocupacion'])),
									'actgironegocio' => utf8_decode(utf8_encode($reg['actgironegocio'])),
									'nacionalidad' => utf8_decode($reg['nacionalidad']),
									'nivelestudios' => utf8_decode($reg['nivelestudios']),
									'calle' =>  caracteresEspeciales($reg['calle']),
									'colonia' => caracteresEspeciales(utf8_decode(utf8_encode($reg['colonia']))),
									'ciudad' => utf8_decode(utf8_encode($reg['ciudad'])),
									'delegacionmunicipio' => utf8_decode(utf8_encode($reg['delegacionmunicipio'])),
									'entidadfederativa' => utf8_decode(utf8_encode($reg['entidadfederativa'])),
									'pais' => utf8_decode($reg['pais']),
									'email' => utf8_decode($reg['email']),
									'numeroexterior' => $reg['numeroexterior'],
									'numerointerior' => $reg['numerointerior'],
									'codigopostal' => $reg['codigopostal'],
									'telefono1' => $reg['telefono1'],
									'tipotelefono1' => utf8_decode($reg['tipotelefono1']),
									'telefono2' => $reg['telefono2'],
									'tipotelefono2' => utf8_decode($reg['tipotelefono2']),
									'calle_trab' => caracteresEspeciales($reg['calletrab']),
									'colonia_trab' => utf8_decode(utf8_encode($reg['coloniatrab'])),
									'ciudad_trab' => utf8_decode(utf8_encode($reg['ciudadtrab'])),
									'delegacionmunicipio_trab' => utf8_decode(utf8_encode($reg['delegacionmunicipiotrab'])),
									'entidadfederativa_trab' => utf8_decode(utf8_encode($reg['entidadfederativatrab'])),
									'pais_trab' => utf8_decode($reg['paistrab']),

									'numeroexterior_trab' => $reg['numeroexteriortrab'],
									'numerointerior_trab' => $reg['numerointeriortrab'],
									'codigopostal_trab' => $reg['codigopostaltrab'],
									'telefono1_trab' => $reg['telefono1trab'],
									'tipotelefono1_trab' => utf8_decode($reg['tipotelefono1trab']),
									'nombres_ref1' => strpos(strtoupper($reg['nombresref1']), 'Ñ')===false ? $reg['nombresref1'] : utf8_decode($reg['nombresref1']),
									'paterno_ref1' => strpos(strtoupper($reg['paternoref1']), 'Ñ')===false ? $reg['paternoref1'] : utf8_decode($reg['paternoref1']),
									'materno_ref1' => strpos(strtoupper($reg['maternoref1']), 'Ñ')===false ? $reg['maternoref1'] : utf8_decode($reg['maternoref1']),
									'curp_ref1' => utf8_decode($reg['curpref1']),
									'tel_ref1' => $reg['telref1'],

									'parentesco_ref1' => utf8_decode($reg['parentescoref1']),
									'nombres_ref2' => strpos(strtoupper($reg['nombresref2']), 'Ñ')===false ? $reg['nombresref2'] : utf8_decode($reg['nombresref2']),
									'paterno_ref2' => strpos(strtoupper($reg['paternoref2']), 'Ñ')===false ? $reg['paternoref2'] : utf8_decode($reg['paternoref2']),
									'materno_ref2' => strpos(strtoupper($reg['maternoref2']), 'Ñ')===false ? $reg['maternoref2'] : utf8_decode($reg['maternoref2']),
									'curp_ref2' => utf8_decode($reg['curpref2']),
									'tel_ref2' => $reg['telref2'],

									'parentesco_ref2' => utf8_decode($reg['parentescoref2']),
									'nombres_prom' => strpos(strtoupper($reg['nombresprom']), 'Ñ')===false ? $reg['nombresprom'] : utf8_decode($reg['nombresprom']),
									'paterno_prom' => strpos(strtoupper($reg['paternoprom']), 'Ñ')===false ? $reg['paternoprom'] : utf8_decode($reg['paternoprom']),
									'materno_prom' => strpos(strtoupper($reg['maternoprom']), 'Ñ')===false ? $reg['maternoprom'] : utf8_decode($reg['maternoprom']),
									'curp_prom' => utf8_decode($reg['curpprom']),
									'folioconsttraspreg' => $reg['folioconst'],
									'cons_implica' => $reg['cons_implica'],
									'edo_cuenta' => $reg['edo_cuenta'],
									'afore_cedente' => $reg['afore_cedente'],
									'numregistro' => $reg['numregistro'],
									//SE AGREGA INSTRUCCION PARA EL TRATADO DE Ñ
									'nombres_benf1' => strpos(strtoupper($reg['nombresbenf1']), 'Ñ')===false ? $reg['nombresbenf1'] : utf8_decode($reg['nombresbenf1']),
									'paterno_benf1' => strpos(strtoupper($reg['paternobenf1']), 'Ñ')===false ? $reg['paternobenf1'] : utf8_decode($reg['paternobenf1']),
									'materno_benf1' => strpos(strtoupper($reg['maternobenf1']), 'Ñ')===false ? $reg['maternobenf1'] : utf8_decode($reg['maternobenf1']),
									'curp_benf1' => utf8_decode($reg['curpbenf1']),
									'porcentaje_benef1' => $reg['porcentajebenef1'],
									'parentesco_benef1' => utf8_decode($reg['parentescobenef1']),

									'nombres_benf2' => strpos(strtoupper($reg['nombresbenf2']), 'Ñ')===false ? $reg['nombresbenf2'] : utf8_decode($reg['nombresbenf2']),
									'paterno_benf2' => strpos(strtoupper($reg['paternobenf2']), 'Ñ')===false ? $reg['paternobenf2'] : utf8_decode($reg['paternobenf2']),
									'materno_benf2' => strpos(strtoupper($reg['maternobenf2']), 'Ñ')===false ? $reg['maternobenf2'] : utf8_decode($reg['maternobenf2']),
									'curp_benf2' => utf8_decode($reg['curpbenf2']),
									'porcentaje_benef2' => $reg['porcentajebenef2'],
									'parentesco_benef2' => utf8_decode($reg['parentescobenef2']),

									'nombres_benf3' => strpos(strtoupper($reg['nombresbenf3']), 'Ñ')===false ? $reg['nombresbenf3'] : utf8_decode($reg['nombresbenf3']),
									'paterno_benf3' => strpos(strtoupper($reg['paternobenf3']), 'Ñ')===false ? $reg['paternobenf3'] : utf8_decode($reg['paternobenf3']),
									'materno_benf3' => strpos(strtoupper($reg['maternobenf3']), 'Ñ')===false ? $reg['maternobenf3'] : utf8_decode($reg['maternobenf3']),
									'curp_benf3' => utf8_decode($reg['curpbenf3']),
									'porcentaje_benef3' => $reg['porcentajebenef3'],
									'parentesco_benef3' => utf8_decode($reg['parentescobenef3']),

									'nombres_benf4' => strpos(strtoupper($reg['nombresbenf4']), 'Ñ')===false ? $reg['nombresbenf4'] : utf8_decode($reg['nombresbenf4']),
									'paterno_benf4' => strpos(strtoupper($reg['paternobenf4']), 'Ñ')===false ? $reg['paternobenf4'] : utf8_decode($reg['paternobenf4']),
									'materno_benf4' => strpos(strtoupper($reg['maternobenf4']), 'Ñ')===false ? $reg['maternobenf4'] : utf8_decode($reg['maternobenf4']),
									'curp_benf4' => utf8_decode($reg['curpbenf4']),
									'porcentaje_benef4' => $reg['porcentajebenef4'],
									'parentesco_benef4' => utf8_decode($reg['parentescobenef4']),

									'nombres_benf5' => strpos(strtoupper($reg['nombresbenf5']), 'Ñ')===false ? $reg['nombresbenf5'] : utf8_decode($reg['nombresbenf5']),
									'paterno_benf5' => strpos(strtoupper($reg['paternobenf5']), 'Ñ')===false ? $reg['paternobenf5'] : utf8_decode($reg['paternobenf5']),
									'materno_benf5' =>strpos(strtoupper($reg['maternobenf5']), 'Ñ')===false ? $reg['maternobenf5'] : utf8_decode($reg['maternobenf5']),
									'curp_benf5' => utf8_decode($reg['curpbenf5']),
									'porcentaje_benef5' => $reg['porcentajebenef5'],
									'parentesco_benef5' => utf8_decode($reg['parentescobenef5']),
									);

				$arrDatosSolicitudNew = array_map('trim', $arrDatosSolicitud);
				}// END FOREACH
			}
			else
			{
				$arrErr = $cnxBd->errorInfo();
				echo ' Error: ' . $arrErr[0] . '-' . $arrErr[1] . '-' . $arrErr[2];
			}
		} //VALIDAR EXISTE FOLIO

		if($iContinuar > 0)
		{
			$sSql = "";
			$sSql = "select curp from colgenerasolicitudregtrasp(".$iFolio.");";
			$resulSet = $cnxBd->query($sSql);
			if($resulSet)
			{

				foreach($resulSet as $reg)
				{
					$curpTrabajador =  $reg['curp'];
				}
			}
			else
			{
				$arrErr = $cnxBd->errorInfo();
				echo ' Error: ' . $arrErr[0] . '-' . $arrErr[1] . '-' . $arrErr[2];
			}
		}

	}
	else
	{
		echo 'parametros incorrectos<br>';
	}

	if($iContinuar > 0)
	{
		switch($hoja)
		{
			case 1:
				generarSolicitudRegistro($arrDatosSolicitudNew,$cMotivoRegTras,$cTelTrabajo,$iFirma,$iFolio,$curpTrabajador);
				break;
			case 2:
				generarSolicitudRegistroHoja2($arrDatosSolicitud, $iFirma, $iFolio);	
				break;
			case 3:
				generarSolicitudRegistroHoja3($arrDatosSolicitud, $iFirma, $iFolio);	
				break;
			case 4:
				generarSolicitudRegistroHoja4($arrDatosSolicitud, $iFirma, $iFolio);	
				break;
			
		}
		
	}
	else
	{
			echo ' El Folio Solicitado ['.$iFolio.'] No pertenese a una Solicitud de Registro ';
	}

}
else
{
	echo 'Error, al abrir conexion a BD postgres';
}

function generarSolicitudRegistro($arrDatosSolicitud,$cMotivo,$cTelTrab,$iFirma,$iFolio,$curpTrabajador)
{
	global $excepcion;
	global $tiposolicitante;
	validartiposolicitanteexcepcion($iFolio);

	//se manda llamar la funcion de eduardo
	consultarHuellas($curpTrabajador, $iFolio);
	$cTelefono1 = '';
	$cTelefono2 = '';
	$cTel_ref1 = '';
	$cTel_ref2 = '';
	$iPidActual = getmypid();

	if(substr($arrDatosSolicitud['telefono1'],0,2) == '55' || substr($arrDatosSolicitud['telefono1'],0,2) == '81' || substr($arrDatosSolicitud['telefono1'],0,2) == '33' )
	{
		$cTelefono1 = ' '.$arrDatosSolicitud['telefono1'];
	}
	else
	{
		$cTelefono1 =  $arrDatosSolicitud['telefono1'];
	}

	if(substr($arrDatosSolicitud['telefono2'],0,2) == '55' ||  substr($arrDatosSolicitud['telefono2'],0,2) == '81' || substr($arrDatosSolicitud['telefono2'],0,2) == '33' )
	{
		$cTelefono2 = ' '.$arrDatosSolicitud['telefono2'];
	}
	else
	{
		$cTelefono2 =  $arrDatosSolicitud['telefono2'];
	}

	if(substr($arrDatosSolicitud['tel_ref1'],0,2) == '55' ||  substr($arrDatosSolicitud['tel_ref1'],0,2) == '81' || substr($arrDatosSolicitud['tel_ref1'],0,2) == '33' )
	{
		$cTel_ref1 = ' '.$arrDatosSolicitud['tel_ref1'];
	}
	else
	{
		$cTel_ref1 =  $arrDatosSolicitud['tel_ref1'];
	}

	if(substr($arrDatosSolicitud['tel_ref2'],0,2) == '55' ||  substr($arrDatosSolicitud['tel_ref2'],0,2) == '81' || substr($arrDatosSolicitud['tel_ref2'],0,2) == '33' )
	{
		$cTel_ref2 = ' '.$arrDatosSolicitud['tel_ref2'];
	}
	else
	{
		$cTel_ref2 =  $arrDatosSolicitud['tel_ref2'];
	}

		$respuesta = new stdClass();
		$pdf = new PDF_Code39();
		$pdf->AliasNbPages();
		$pdf->AddPage('P','A4');
		$iAltoRen = 5.5;
		$iSaltoLinea = 5.5;
		$sizeFontTitulo = 14;
		$sizeFontCuerpo = 11;
		$sizeFontPiePagina = 6;
		$iRadio = 1;
		$iBordoCel = 0;
		$iBorder = 10;
		$FinMargen = 203;
		$iAltoRen2 = 1;
		$InicioMargen = 4;
		$iAltoRenCabecera = 5.5;
		$iSaltoLineaCabecera = 5;
		$sizeFontCuadro = 11;
		$sizeFontCuerpo2 = 6;
		$SangriaMargen = 5.5;
		$interliniadoCuerpo = 2.6;
		$iSaltoLinea2 = 1;
		$iSaltoLineaExtra=0.3;
		$iSaltoLineaC = 3.5;

		//variables auxiliares
		$Lada1 = '';
		$Tel1 = '';
		$Lada2 = '';
		$Tel2 = '';
		$LadaBenef1 = '';
		$TelBenef1 = '';
		$LadaBenef2 = '';
		$TelBenef2 = '';


			//Decodificar el arreglo para mostrar correctamente los acentos
			foreach ($arrDatosSolicitud as $key=>$valor)
			{
				$arrDatosSolicitud[$key] = $arrDatosSolicitud[$key];
			}
			//Establecer margenes izquierdo, arriba, derecho
			$pdf->SetMargins(4, 4 ,4);

			$pdf->SetFillColor(80,80,80); //Se establece el color de relleno para el encabezado
			$pdf->SetTextColor(255,255,255); //Se establece el color de fuente blanco para el encabezado
			$pdf->SetFont('Arial','B',$sizeFontTitulo);

			$pdf->RoundedRect(4, $pdf->getY(), 203, $iAltoRen * 1.9 , $iRadio, 'DF', '1234');
			$pdf->setX(4);
			$pdf->Cell( 0, $iAltoRen *2 , 'SOLICITUD DE REGISTRO EN ADMINISTRADORA DE FONDOS PARA EL RETIRO' , $iBorder, 0, 'D');
			$pdf->SetTextColor(0,0,0);

			$pdf->Ln($iSaltoLinea * 2);
			$pdf->SetFont('Arial','',$sizeFontCuerpo);
			$pdf->Cell( 0, $iAltoRen, utf8_decode("Al firmar, está eligiendo a AFORE COPPEL para que administre su cuenta individual") , $iBorder, 0, 'L');
			$pdf->Ln($iSaltoLinea);

			$pdf->Cell(150);
			$pdf->Cell(30, $iAltoRen, 'Fecha Firma: ' , $iBorder, 0, 'R');
			$pdf->RoundedRect($pdf->getX() - 1, $pdf->getY(), 24, $iAltoRen - 0.5 , $iRadio, 'D', '1234');
			$pdf->Cell(25, $iAltoRen, $arrDatosSolicitud['fechafirma'] , $iBorder, 0, 'L');
			$pdf->Ln($iSaltoLinea);

	//		$pdf->Cell(39, $iAltoRen, 'Motivo del Registro: ' , $iBorder, 0, 'L');
	//		$pdf->Line($pdf->getX()+1, $pdf->getY() + $iAltoRen - 0.5 , $pdf->getX()+106.5, $pdf->getY() + $iAltoRen - 0.5);
	//		$pdf->Cell(111, $iAltoRen, $cMotivo , $iBorder, 0, 'L');
			$pdf->Cell(150);
			$pdf->Cell(30, $iAltoRen, 'Folio: ' , $iBorder, 0, 'R');
			$pdf->RoundedRect($pdf->getX() - 1, $pdf->getY(), 24, $iAltoRen - 0.5, $iRadio, 'D', '1234');
			$pdf->Cell(25, $iAltoRen, $arrDatosSolicitud['folio'] , $iBorder, 0, 'L');
			$pdf->Ln($iSaltoLinea);

			$pdf->Cell(35, $iAltoRen, 'Afore Receptora: ' , $iBorder, 0, 'L');
			$pdf->Line($pdf->getX(), $pdf->getY() + $iAltoRen - 0.5 , $pdf->getX() + 40, $pdf->getY() + $iAltoRen - 0.5);
			$pdf->SetFont('Arial','B',$sizeFontCuerpo);

			$pdf->Cell(40, $iAltoRen, 'AFORE COPPEL' , $iBorder, 0, 'L');
			$pdf->SetFont('Arial','',$sizeFontCuerpo);
			$pdf->Cell(35, $iAltoRen, utf8_decode('Fecha Impresión: ') ,  $iBorder, 0, 'L');
			$pdf->Line($pdf->getX(), $pdf->getY() + $iAltoRen - 0.5 , $pdf->getX() + 35.5, $pdf->getY() + $iAltoRen - 0.5);
			$pdf->Cell(37, $iAltoRen, $arrDatosSolicitud['fechaimpresion'] , $iBorder, 0, 'L');

			$pdf->Cell(33, $iAltoRen, 'Fecha Vigencia: ' , $iBorder, 0, 'L');
			$pdf->RoundedRect($pdf->getX() - 1, $pdf->getY(), 24, $iAltoRen - 0.5, $iRadio, 'D', '1234');
			$pdf->Cell(25, $iAltoRen, $arrDatosSolicitud['fechavigencia'] , $iBorder, 0, 'L');
			$pdf->Ln($iSaltoLinea * 1.2);

			$pdf->SetFont('Arial','B',$sizeFontCuerpo);
			$pdf->Cell( 208, $iAltoRen, 'DATOS DEL TRABAJADOR' , $iBorder, 0, 'C');
			$pdf->SetFont('Arial','',$sizeFontCuerpo);
			$pdf->Ln($iSaltoLinea);

			$pdf->RoundedRect(4, $pdf->getY() , 118, $iAltoRen * 4.2 , $iRadio, 'D', '1234' );
			$pdf->RoundedRect(124, $pdf->getY(), 83, $iAltoRen * 4.2 , $iRadio, 'D', '1234' );

			$pdf->Cell( 19, $iAltoRen, ' Nombre: ' , $iBorder/**/, 0, 'L');
			$pdf->Line($pdf->getX(), $pdf->getY() + $iAltoRen - 0.5 , $pdf->getX() + 97, $pdf->getY() + $iAltoRen - 0.5);
			$pdf->Cell( 103, $iAltoRen, $arrDatosSolicitud['nombre'] , $iBorder/**/, 0, 'L');

			$pdf->Cell( 15, $iAltoRen, 'CURP: ' , $iBorder/**/, 0, 'L');
			$pdf->Line($pdf->getX(), $pdf->getY() + $iAltoRen - 0.5 , $pdf->getX() + 64, $pdf->getY() + $iAltoRen - 0.5);
			$pdf->Cell( 61, $iAltoRen, $arrDatosSolicitud['curp'] , $iBorder/**/, 0, 'L');
			$pdf->Ln($iSaltoLinea);

			$pdf->Cell( 33, $iAltoRen, ' Primer Apellido: ' , $iBorder/**/, 0, 'L');
			$pdf->Line($pdf->getX(), $pdf->getY() + $iAltoRen - 0.5 , $pdf->getX() + 83, $pdf->getY() + $iAltoRen - 0.5);
			$pdf->Cell( 89, $iAltoRen, $arrDatosSolicitud['paterno'] , $iBorder/**/, 0, 'L');

			$pdf->Cell( 12, $iAltoRen, 'NSS: ' , $iBorder/**/, 0, 'L');
			$pdf->Line($pdf->getX(), $pdf->getY() + $iAltoRen - 0.5 , $pdf->getX() + 67, $pdf->getY() + $iAltoRen - 0.5);
			$pdf->Cell( 69, $iAltoRen, $arrDatosSolicitud['nss'] , $iBorder/**/, 0, 'L');
			$pdf->Ln($iSaltoLinea);

			$pdf->Cell( 37, $iAltoRen, ' Segundo Apellido: ' , $iBorder/**/, 0, 'L');
			$pdf->Line($pdf->getX(), $pdf->getY() + $iAltoRen - 0.5 , $pdf->getX() + 79, $pdf->getY() + $iAltoRen - 0.5);
			$pdf->Cell( 85, $iAltoRen, $arrDatosSolicitud['materno'] , $iBorder/**/, 0, 'L');

			$pdf->Cell( 12, $iAltoRen, 'RFC: ' , $iBorder/**/, 0, 'L');
			$pdf->Line($pdf->getX(), $pdf->getY() + $iAltoRen - 0.5 , $pdf->getX() + 67, $pdf->getY() + $iAltoRen - 0.5);
			$pdf->Cell( 69, $iAltoRen, $arrDatosSolicitud['rfc'] , $iBorder/**/, 0, 'L');
			$pdf->Ln($iSaltoLinea);

			$pdf->Cell(122);
			$pdf->Cell( 24, $iAltoRen, 'Fecha Nac: ' , $iBorder/**/, 0, 'L');
			$pdf->Line($pdf->getX(), $pdf->getY() + $iAltoRen - 0.5 , $pdf->getX() + 28, $pdf->getY() + $iAltoRen - 0.5);
			$pdf->Cell( 24, $iAltoRen, $arrDatosSolicitud['fechanacimiento'] , $iBorder/**/, 0, 'L');
			$pdf->Cell( 22, $iAltoRen, utf8_decode('Género: ') , 0/**/, 0, 'R');
			$pdf->Line($pdf->getX(), $pdf->getY() + $iAltoRen - 0.5 , $pdf->getX() + 9, $pdf->getY() + $iAltoRen - 0.5);
			$pdf->Cell( 9, $iAltoRen, $arrDatosSolicitud['genero'] , $iBorder/**/, 0, 'C');
			$pdf->Ln($iSaltoLinea * 1.5);

			$pdf->RoundedRect(4, $pdf->getY() , 108, $iAltoRen * 2.2, $iRadio, 'D', '1234' );
			$pdf->RoundedRect(114, $pdf->getY() , 93, $iAltoRen * 2.2, $iRadio, 'D', '1234' );

			$pdf->Cell( 23, $iAltoRen, utf8_decode(' Ocupación: ') , $iBorder/**/, 0, 'L');
			$pdf->Line($pdf->getX(), $pdf->getY() + $iAltoRen - 0.5 , $pdf->getX() + 83, $pdf->getY() + $iAltoRen - 0.5);
			$pdf->Cell( 89, $iAltoRen, $arrDatosSolicitud['ocupacion'], $iBorder/**/, 0, 'L');
			$pdf->Cell( 27, $iAltoRen, 'Nacionalidad:' , $iBorder/**/, 0, 'L');
			$pdf->Line($pdf->getX(), $pdf->getY() + $iAltoRen - 0.5 , $pdf->getX() + 62, $pdf->getY() + $iAltoRen - 0.5);
			$pdf->Cell( 63, $iAltoRen, $arrDatosSolicitud['nacionalidad'] , $iBorder/**/, 0, 'L');
			$pdf->Ln($iSaltoLinea);

			$pdf->Cell( 46, $iAltoRen, ' Act. o Giro del Negocio: ' , 0/**/, 0, 'L');
			$pdf->Line($pdf->getX(), $pdf->getY() + $iAltoRen - 0.5 , $pdf->getX() + 60, $pdf->getY() + $iAltoRen - 0.5);
			$pdf->Cell( 69, $iAltoRen, $arrDatosSolicitud['actgironegocio'] , 0/**/, 0, 'L');
			$pdf->Cell( 33, $iAltoRen, 'Nivel de Estudios:' , 0/**/, 0, 'R');
			$pdf->Line($pdf->getX(), $pdf->getY() + $iAltoRen - 0.5 , $pdf->getX() + 53, $pdf->getY() + $iAltoRen - 0.5);
			$pdf->Cell( 53, $iAltoRen, $arrDatosSolicitud['nivelestudios'] , 0/**/, 0, 'L');
			$pdf->Ln($iSaltoLinea *1.5);

			$pdf->SetFont('Arial','B',$sizeFontCuerpo);
			$pdf->Cell( 203, $iAltoRen, 'DOMICILIO DEL TRABAJADOR' , 0, 0, 'C');
			$pdf->SetFont('Arial','',$sizeFontCuerpo);
			$pdf->Ln($iSaltoLinea );

			$pdf->RoundedRect(4, $pdf->getY() , 203, $iAltoRen * 6.2, $iRadio, 'D', '1234' );
			$pdf->Line($pdf->getX() + 159, $pdf->getY(), $pdf->getX() + 159, $pdf->getY() + $iAltoRen * 6.2);//--

			$pdf->Cell( 14, $iAltoRen, ' Calle: ' , 0/**/, 0, 'L');
			$pdf->Line($pdf->getX(), $pdf->getY() + $iAltoRen - 0.5 , $pdf->getX() + 143, $pdf->getY() + $iAltoRen - 0.5);
			$pdf->Cell( 143, $iAltoRen, $arrDatosSolicitud['calle'] , 0/**/, 0, 'L');

			$pdf->Cell( 19, $iAltoRen, 'N. Ext.' , 0/**/, 0, 'R');
			$pdf->Line($pdf->getX(), $pdf->getY() + $iAltoRen - 0.5 , $pdf->getX() + 25, $pdf->getY() + $iAltoRen - 0.5);
			$pdf->Cell( 25, $iAltoRen, $arrDatosSolicitud['numeroexterior'] , 0/**/, 0, 'L');
			$pdf->Ln($iSaltoLinea);

			$pdf->Cell( 18, $iAltoRen, ' Colonia: ' , 0/**/, 0, 'L');
			$pdf->Line($pdf->getX(), $pdf->getY() + $iAltoRen - 0.5 , $pdf->getX() + 139, $pdf->getY() + $iAltoRen - 0.5);
			$pdf->Cell( 139, $iAltoRen, $arrDatosSolicitud['colonia'] , 0/**/, 0, 'L');

			$pdf->Cell( 19, $iAltoRen, 'N. Int.' , 0/**/, 0, 'R');
			$pdf->Line($pdf->getX(), $pdf->getY() + $iAltoRen - 0.5 , $pdf->getX() + 25, $pdf->getY() + $iAltoRen - 0.5);
			$pdf->Cell( 25, $iAltoRen, $arrDatosSolicitud['numerointerior'] , 0/**/, 0, 'L');
			$pdf->Ln($iSaltoLinea);

			$pdf->Cell( 17, $iAltoRen, ' Ciudad: ' , 0/**/, 0, 'L');
			$pdf->Line($pdf->getX(), $pdf->getY() + $iAltoRen - 0.5 , $pdf->getX() + 140, $pdf->getY() + $iAltoRen - 0.5);
			$pdf->Cell( 140, $iAltoRen, $arrDatosSolicitud['ciudad'] , 0/**/, 0, 'L');

			$pdf->Cell( 19, $iAltoRen, 'C.P.' , 0/**/, 0, 'R');
			$pdf->Line($pdf->getX(), $pdf->getY() + $iAltoRen - 0.5 , $pdf->getX() + 25, $pdf->getY() + $iAltoRen - 0.5);
			$pdf->Cell( 25, $iAltoRen, $arrDatosSolicitud['codigopostal'] , 0/**/, 0, 'L');
			$pdf->Ln($iSaltoLinea);

			$pdf->Cell( 33, $iAltoRen, ' Del. o Municipio: ' , 0/**/, 0, 'L');
			$pdf->Line($pdf->getX(), $pdf->getY() + $iAltoRen - 0.5 , $pdf->getX() + 124, $pdf->getY() + $iAltoRen - 0.5);
			$pdf->Cell( 162, $iAltoRen, $arrDatosSolicitud['delegacionmunicipio'] , 0/**/, 0, 'L');
			$pdf->Ln($iSaltoLinea);

			$pdf->Cell( 39, $iAltoRen, ' Entidad Federativa: ' , 0/**/, 0, 'L');
			$pdf->Line($pdf->getX(), $pdf->getY() + $iAltoRen - 0.5 , $pdf->getX() + 46, $pdf->getY() + $iAltoRen - 0.5);
			$pdf->Cell( 46, $iAltoRen, $arrDatosSolicitud['entidadfederativa'] , 0/**/, 0, 'L');

			$pdf->Cell( 11, $iAltoRen, utf8_decode('País: ') , 0/**/, 0, 'L');
			$pdf->Line($pdf->getX(), $pdf->getY() + $iAltoRen - 0.5 , $pdf->getX() + 21, $pdf->getY() + $iAltoRen - 0.5);
			$pdf->Cell( 21, $iAltoRen, utf8_decode('MÉXICO') , 0/**/, 0, 'L');

			$pdf->Cell( 11.5, $iAltoRen, 'Tel.1:(      )' , 0/**/, 0, 'L');
			$pdf->Line($pdf->getX(), $pdf->getY() + $iAltoRen - 0.5 , $pdf->getX() + 27.5, $pdf->getY() + $iAltoRen - 0.5);
			$Lada1 = substr($cTelefono1,0,3);
			$Tel1 = substr($cTelefono1,3,10);
			$pdf->Cell( 8.3, $iAltoRen,$Lada1 , 0/**/, 0, 'L');
			$pdf->Cell( 17, $iAltoRen,$Tel1 , 0/**/, 0, 'L');

			$pdf->Cell( 19, $iAltoRen, 'Tipo:' , 0/**/, 0, 'R');
			$pdf->Line($pdf->getX(), $pdf->getY() + $iAltoRen - 0.5 , $pdf->getX() + 25, $pdf->getY() + $iAltoRen - 0.5);
			$pdf->Cell( 21, $iAltoRen, $arrDatosSolicitud['tipotelefono1'] , 0/**/, 0, 'L');
			$pdf->Ln($iSaltoLinea);

			$pdf->Cell( 21, $iAltoRen, ' Correo E. ' , 0/**/, 0, 'L');
			$pdf->Line($pdf->getX(), $pdf->getY() + $iAltoRen - 0.5 , $pdf->getX() + 96, $pdf->getY() + $iAltoRen - 0.5);
			$pdf->Cell( 96, $iAltoRen, $arrDatosSolicitud['email'] , 0/**/, 0, 'L');

			$pdf->Cell( 12.2, $iAltoRen, 'Tel.2:(      )' , 0/**/, 0, 'L');
			$pdf->Line($pdf->getX(), $pdf->getY() + $iAltoRen - 0.5 , $pdf->getX() + 27.5, $pdf->getY() + $iAltoRen - 0.5);
			$Lada2 = substr($cTelefono2,0,3);
			$Tel2 = substr($cTelefono2,3,10);
			$pdf->Cell( 8.3, $iAltoRen, $Lada2 , 0/**/, 0, 'L');
			$pdf->Cell( 17, $iAltoRen, $Tel2  , 0/**/, 0, 'L');

			$pdf->Cell( 19, $iAltoRen, 'Tipo:' , 0/**/, 0, 'R');
			$pdf->Line($pdf->getX(), $pdf->getY() + $iAltoRen - 0.5 , $pdf->getX() + 25, $pdf->getY() + $iAltoRen - 0.5);
			$pdf->Cell( 21, $iAltoRen, $arrDatosSolicitud['tipotelefono2'] , 0/**/, 0, 'L');
			$pdf->Ln($iSaltoLinea *1.5);
// Domicilio Laboral
		

// referencias personales 

			$pdf->RoundedRect(4, $pdf->getY() , 96, $iAltoRen * 5 , $iRadio, 'D', '1234');
			$pdf->RoundedRect(104, $pdf->getY() , 103, $iAltoRen * 5 , $iRadio, 'D', '1234');

			$pdf->Ln($iSaltoLinea * 5);
			$pdf->SetFont('Arial','',10);
			$pdf->Cell( 100, $iAltoRen, 'Firma del Agente Promotor' , $iBorder, 0, 'L');
			$pdf->Cell( 3 ,$iAltoRen, '' , 0, 0, 'C');
			$pdf->Cell( 100, $iAltoRen, 'Firma del Trabajador' , $iBorder, 0, 'R');

			$pdf->Ln($iSaltoLinea * .2);
			$pdf->SetFont('Arial','B',$sizeFontCuerpo);
			$pdf->Cell( 203, $iAltoRen, 'DATOS DEL AGENTE PROMOTOR' , 0, 0, 'C');
			$pdf->SetFont('Arial','',$sizeFontCuerpo);
			$pdf->Ln($iSaltoLinea);

			$pdf->RoundedRect(4, $pdf->getY() , 203, ($iAltoRen * 2.2) , $iRadio, 'D', '1234');

			$pdf->Cell( 18, $iAltoRen, ' Nombre: ' , 0/**/, 0, 'L');
			$pdf->Line($pdf->getX(), $pdf->getY() + $iAltoRen - 0.5 , $pdf->getX() + 182, $pdf->getY() + $iAltoRen - 0.5);
			$pdf->Cell( 22.5, $iAltoRen, $arrDatosSolicitud['nombres_prom']. ' ' .$arrDatosSolicitud['paterno_prom'].' '.$arrDatosSolicitud['materno_prom'] , 0/**/, 0, 'L');
			$pdf->Ln($iSaltoLinea);

			$pdf->Cell(16, $iAltoRen, ' CURP: ' , 0/**/, 0, 'L');
			$pdf->Line($pdf->getX(), $pdf->getY() + $iAltoRen - 0.5 , $pdf->getX() + 60, $pdf->getY() + $iAltoRen - 0.5);
			$pdf->Cell( 60, $iAltoRen, $arrDatosSolicitud['curp_prom'] , 0/**/, 0, 'L');


			$pdf->Cell(29, $iAltoRen, utf8_decode('Núm. Registro: ') , 0/**/, 0, 'L');
			$pdf->Line($pdf->getX(), $pdf->getY() + $iAltoRen - 0.5 , $pdf->getX() +95 , $pdf->getY() + $iAltoRen - 0.5);
			$pdf->Cell( 105, $iAltoRen, $arrDatosSolicitud['numregistro'] , 0/**/, 0, 'L');
			$pdf->Ln($iSaltoLinea * 1.5);

			$pdf->RoundedRect(4, $pdf->getY() , 122, $iAltoRen * 1.1, $iRadio, 'D', '1234');


			$pdf->Cell( 45, $iAltoRen, ' F. Solicitud Registro: ' , 0/**/, 0, 'L');
			$pdf->Line($pdf->getX(), $pdf->getY() + $iAltoRen - 0.5 , $pdf->getX() + 75, $pdf->getY() + $iAltoRen - 0.5);
			$pdf->Cell( 75, $iAltoRen, $arrDatosSolicitud['folioconsttraspreg'] , 0/**/, 0, 'L');
			$pdf->Ln($iSaltoLinea * 0.4);
			
			$auxY = $pdf->getY();
			$sRutaNombreTrabajador = $arrDatosSolicitud['nombre'].' '.$arrDatosSolicitud['paterno'].' '.$arrDatosSolicitud['materno'];
	        $pdf->SetFont('Arial','B', 7.5);
			$pdf->SetTextColor(0,0,0);
			$pdf->SetXY(112, 149.1);
			$pdf->Write(12, $sRutaNombreTrabajador);
		   
		   $pdf->SetY($auxY);
		        //1425.1 UZIEL GENARO 
			$pdf->SetFont('Tahoma','B',$sizeFontCuadro);
			$pdf->RoundedRect(4, $pdf->getY()+ $iSaltoLineaCabecera *1.2, 203, $iAltoRenCabecera * 5, $iRadio, 'D', '1234' );
			$pdf->SetY($pdf->getY()+ $iSaltoLineaCabecera *1.5);
			$pdf->setX($InicioMargen);
			$pdf->MultiCell( $FinMargen, 5, utf8_decode("CONTRATO  DE  ADMINISTRACIÓN  DE  FONDOS  PARA  EL  RETIRO  QUE  CELEBRAN  POR  UNA
				PARTE  AFORE  COPPEL,  S.A.  DE  C.V.,  A  QUIEN  EN  LO  SUCESIVO  SE  LE  DENOMINARÁ LA
			".CHR(34)."AFORE".CHR(34)." Y POR  LA OTRA LA  PERSONA FÍSICA CUYOS DATOS  APARECEN  EN  LA  SOLICITUD
			DE  REGISTRO O DE TRASPASO  ADJUNTA  A  QUIEN  EN LO SUCESIVO SE LE DENOMINARÁ EL
			".CHR(34)."TRABAJADOR".CHR(34)."; AL  TENOR  DE  LAS  SIGUIENTES  DECLARACIONES  Y  CLÁUSULAS:"), $iBorder, 'C');

			$pdf->Ln($iSaltoLineaC);
			$pdf->SetFont('Tahoma','B',$sizeFontCuerpo2);
			$pdf->Cell( 203, $iAltoRen2, 'D E C L A R A C I O N E S' ,  $iBorder/**/, 0, 'C');

			$pdf->Ln($iSaltoLinea2);
			$pdf->Cell( 203, $iAltoRen2, 'I.	EL TRABAJADOR DECLARA:' , $iBorder/**/, 0, 'L');
			$pdf->Ln($iSaltoLinea2+$iSaltoLineaExtra);

			$pdf->SetFont('Tahoma','',$sizeFontCuerpo2);
			$pdf->setX($SangriaMargen);
				//194.8
			$pdf->MultiCell(($FinMargen +$InicioMargen) -$SangriaMargen, $interliniadoCuerpo, utf8_decode("a)  Identificarse bajo el nombre ").$arrDatosSolicitud ['nombre']." ".$arrDatosSolicitud['paterno']." ".$arrDatosSolicitud['materno'].utf8_decode(", Registro Federal de Contribuyentes ").$arrDatosSolicitud['rfc'].utf8_decode(", de ocupación ").$arrDatosSolicitud['ocupacion'].utf8_decode(", actividad económica ").$arrDatosSolicitud['actgironegocio'].utf8_decode(", señalando como correo electrónico o bien teléfono celular ").$arrDatosSolicitud['email'].", ".$arrDatosSolicitud['telefono1'].utf8_decode("; en lo sucesivo el TRABAJADOR
b)  Que sus datos generales contenidos en la Solicitud de Registro y/o Traspaso en Administradora de Fondos para el Retiro, forman parte integral del  presente instrumento.
c)  Que reconoce expresamente que por la naturaleza de sus  inversiones en acciones de  la(s) SIEFORE(s) y de aquellas inversiones que esta(s) última(s) realiza(n) en el  mercado  de  valores, inclusive sobre instrumentos de deuda, no es posible garantizar rendimientos y que, por lo tanto, sus inversiones se encuentran sujetas a pérdidas o ganancias que en lo general provienen de fluctuaciones del mercado, y
d)  Que conoce su derecho de elegir libremente la Administradora que operará su Cuenta Individual, derecho que está ejerciendo voluntariamente al firmar o plasmar su huella digital en el presente documento.
e)  Que la AFORE le ha informado sobre el uso y tratamiento de los datos personales que serán recabados en este acto, mediante el aviso de privacidad, el cual fue puesto a su disposición, aceptando EL TRABAJADOR plenamente el contenido y alcance.")
, $iBorder, 'J');
        	$pdf->Ln($iSaltoLinea2);
			$pdf->SetFont('Tahoma','B',$sizeFontCuerpo2);
			$pdf->Cell( 203, $iAltoRen2, 'II. 	LA AFORE DECLARA:' , $iBorder/**/, 0, 'L');
			$pdf->Ln($iSaltoLinea2+$iSaltoLineaExtra);

			$pdf->SetFont('Tahoma','',$sizeFontCuerpo2);
			$pdf->setX($SangriaMargen);

			$pdf->MultiCell(($FinMargen +$InicioMargen) -$SangriaMargen, $interliniadoCuerpo, utf8_decode("a) Que es una Administradora de Fondos para el Retiro, constituida de conformidad con la Ley de los Sistemas de Ahorro para el Retiro y demás disposiciones aplicables, cuyos datos  generales  se encuentran en el anverso de la Solicitud de Registro y/o  de  Traspaso en  Administradora  de  Fondos para el Retiro según sea el caso.
b)  Que cuenta con autorización de la Comisión Nacional de los  Sistemas  de  Ahorro  para  el  Retiro para organizarse, operar y llevar a cabo las funciones propias y exclusivas de una Administradora de Fondos para el Retiro;
c)  Que las facultades de su representante constan en escritura pública y  que  a  la  fecha  de  suscripción de este acuerdo de voluntades no le han sido modificadas, limitadas ni revocadas en forma alguna; y
d)  Que la AFORE opera y administra  la(s)  Sociedad(es)  de  Inversión  Especializada(s)  de  Fondos para el Retiro que le han sido autorizadas por la Comisión Nacional de los Sistemas de Ahorro para el Retiro.
"), $iBorder, 'J');

			$pdf->Ln($iSaltoLinea2);
			$pdf->SetFont('Tahoma','B',$sizeFontCuerpo2);
			$pdf->Cell( 203, $iAltoRen2, 'III. 	DECLARAN AMBAS PARTES:' , $iBorder/**/, 0, 'L');
			$pdf->Ln($iSaltoLinea2+$iSaltoLineaExtra);

			$pdf->SetFont('Tahoma','',$sizeFontCuerpo2);
			$pdf->setX($SangriaMargen);

			$pdf->MultiCell(($FinMargen +$InicioMargen) -$SangriaMargen, $interliniadoCuerpo, utf8_decode("Que la Solicitud de Registro y/o de Traspaso  en Administradora de Fondos para el Retiro, según sea el caso, forma parte integral de este documento y que los datos contenidos en la misma son ciertos.
Expuestas las declaraciones que anteceden, las partes están de acuerdo en celebrar el presente contrato sujetándose a las siguientes:
"), 0, 'J');

			$pdf->SetFont('Tahoma','B',$sizeFontCuerpo2);

			$pdf->Ln($iSaltoLinea2);
			$pdf->Cell( 203, $iAltoRen2, utf8_decode('C L Á U S U L A S') , $iBorder/**/, 0, 'C');

			
			$pdf->setX($SangriaMargen);
			$pdf->Cell( 203, $iAltoRen2, 'PRIMERA.- DEFINICIONES ' , $iBorder/**/, 0, 'L');
			$pdf->Ln($iSaltoLinea2+$iSaltoLineaExtra);

			


			$pdf->SetFont('Tahoma','',$sizeFontCuerpo2);
			$pdf->setX($SangriaMargen);

			$pdf->MultiCell(($FinMargen+$InicioMargen)-$SangriaMargen, $interliniadoCuerpo, utf8_decode("Para efectos de este contrato, se entenderá por:
I. AFORE, a las Administradoras de Fondos de Ahorro para el Retiro;
II. Base de Datos Nacional SAR, aquella conformada por la información procedente de los Sistemas de Ahorro para el Retiro, conteniendo la información individual de cada trabajador y el Registro de la Administradora de Fondos para el Retiro o Institución de Crédito en que cada uno de estos se encuentra afiliado;"), $iBorder, 'J');

            $rutaPdf = "/sysx/progs/web/salida/solicitudafiliacion/".$iPidActual."SolicitudAfiliacionRegistroH1".$arrDatosSolicitud['curp'].".pdf";
			$respuesta->rutaPdf = "/sysx/progs/web/salida/solicitudafiliacion/".$iPidActual."SolicitudAfiliacionRegistroH1".$arrDatosSolicitud['curp'].".pdf";
			$sRutaAbsoluta = $iPidActual."SolicitudAfiliacionRegistroH1".$arrDatosSolicitud['curp'].".pdf";

			//ARCHIVO TEMPORAL
			$rutaPdfTemp = "/sysx/progs/web/salida/solicitudafiliacion/".$arrDatosSolicitud['folio']."_".$iPidActual."SolicitudAfiliacionRegistroH1".$arrDatosSolicitud['curp']."_TEMP.pdf";

			//RUTA DE LAS FIRMAS
			$sRutaFirmaTrabajador = "/sysx/progs/web/entrada/firmas/UFAF_".$arrDatosSolicitud['folio']."_FTRAB.JPG";
			$sRutaFirmaPromotor = "/sysx/progs/web/entrada/firmas/UFAF_".$arrDatosSolicitud['folio']."_FPROM.JPG";

			try
			{
				$sRutaNombreTrabajador = $arrDatosSolicitud['nombre'].' '.$arrDatosSolicitud['paterno'].' '.$arrDatosSolicitud['materno'];
				//$pdf->Image('/sysx/progs/web/entrada/firmas/ST00_'.$arrDatosSolicitud["folio"].'_NTRAB.JPG', 113, 213.5, 70, 12, 'jpg');
				$pdf->SetFont('Arial','B', 7.5);
				$pdf->SetTextColor(0,0,0);
				$pdf->SetXY(112, 149.1);
				$pdf->Write(12, $sRutaNombreTrabajador);
				$pdf->Output($rutaPdf);
				$objMetodosExpedienteAfiliacion = new CMetodosExpedienteAfiliacion();


				if ($iFirma == 1)
				{
						$pdf = new FPDI();

						$pageCount = $pdf->setSourceFile('/sysx/progs/web/salida/solicitudafiliacion/'.$sRutaAbsoluta);
						$tplIdx = $pdf->importPage(1);
						$pdf->addPage();
						$pdf->useTemplate($tplIdx, null, null, 0, 0, true);
						$user_agent = $_SERVER['HTTP_USER_AGENT'];
						$OSname = getPlatform($user_agent);
						if($OSname == "Android"){

							// Envio de correo
							$sDocumento = $sRutaAbsoluta;
							$sUbicacion = "/sysx/progs/web/salida/solicitudafiliacion/";
							$sIpOrigen = $_SERVER["HTTP_HOST"];
							$sTipo = "SR00";

							$arrAPI = array(
								'folio' => $arrDatosSolicitud['folio'],
								'documento' => $sDocumento,
								'ubicacion' => $sUbicacion,
								'iporigen' => $sIpOrigen,
								'tipodocumento' => $sTipo
							);

						}


						//376.1	YPPM
						if (file_exists('/sysx/progs/web/salida/solicitudafiliacion/TRAB_'.$arrDatosSolicitud["folio"].'.jpg'))
						{
							$pdf->Image('/sysx/progs/web/salida/solicitudafiliacion/TRAB_'.$arrDatosSolicitud["folio"].'.jpg', 181, 130, 20, 20, 'jpg');
							$iContinuar = 1;
						}
						else
						{
							$pdf->SetFont('Arial','B', 7);
							$pdf->SetTextColor(0,0,0);
							$pdf->SetXY(185, 152);
							$pdf->Write(0, "Sin huella");
							$iContinuar = 1;
						}
						if (file_exists('/sysx/progs/web/salida/solicitudafiliacion/PROM_'.$arrDatosSolicitud["folio"].'.jpg'))
						{
							$pdf->Image('/sysx/progs/web/salida/solicitudafiliacion/PROM_'.$arrDatosSolicitud["folio"].'.jpg', 73, 130, 20, 20, 'jpg');
							$iContinuar = 1;
						}
						else
						{
							$pdf->SetFont('Arial','B', 7);
							$pdf->SetTextColor(0,0,0);
							$pdf->SetXY(73, 152);
							$pdf->Write(0, "Sin huella");
							$iContinuar = 1;
						}

						CLogImpresion::escribirLog($iContinuar);


					if($iContinuar == 1)
					{
						$pdf->Output($rutaPdf, 'F');
						chmod($rutaPdf, 0777);
						copy($rutaPdf, $rutaPdfTemp);

						$pdf = new FPDI();

						$pageCount = $pdf->setSourceFile('/sysx/progs/web/salida/solicitudafiliacion/'.$sRutaAbsoluta);
						$tplIdx = $pdf->importPage(1);
						$pdf->addPage();
						$pdf->useTemplate($tplIdx, null, null, 0, 0, true);
						$sRutaNombreTrabajador = $arrDatosSolicitud['nombre'].' '.$arrDatosSolicitud['paterno'].' '.$arrDatosSolicitud['materno'];
						//$pdf->Image('/sysx/progs/web/entrada/firmas/ST00_'.$arrDatosSolicitud["folio"].'_NTRAB.JPG', 113, 213.5, 70, 12, 'jpg');
						$pdf->SetFont('Arial','B', 7.5);
						$pdf->SetTextColor(0,0,0);
						$pdf->SetXY(112, 149.1);
						$pdf->Write(12, $sRutaNombreTrabajador);
						//RUTA DE LA FIRMA DEL TRABAJADOR Y DEL PROMOTOR
						$pdf->Image('../entrada/firmas/UFAF_'.$arrDatosSolicitud["folio"].'_FPROM.JPG', 7, 135, 50, 20, 'JPG');
						$pdf->Image('../entrada/firmas/UFAF_'.$arrDatosSolicitud["folio"].'_FTRAB.JPG', 110, 135, 45, 17, 'JPG');

						CLogImpresion::escribirLog("Se agregan las firmas al PDF de REGISTRO");

						//SE GUARDA PDF
						$pdf->Output($rutaPdf, 'F');

						//copy($rutaPdf, $rutaPdfTemp);

						//SE GUARDA PDF
						$pdf->Output($rutaPdf, 'F');

						$user_agent = $_SERVER['HTTP_USER_AGENT'];
						$OSname = getPlatform($user_agent);
						if($OSname == "Android"){

							// create Imagick object
							$imagick = new Imagick();
							// Reads image from PDF
							$imagick->setResolution(500, 500);
							$imagick->readImage($rutaPdf);

							$sRuta_imag = str_replace(".pdf","",$sRutaAbsoluta);

							// Writes an image or image sequence Example- converted-0.jpg, converted-1.jpg
							$imagick->writeImages('/sysx/progs/web/salida/solicitudafiliacion/'.$sRuta_imag.'.tif', false);

						}

						$user_agent = $_SERVER['HTTP_USER_AGENT'];
						$OSname = getPlatform($user_agent);
						if($OSname == "Android"){

							// create Imagick object
							$imagick = new Imagick();
							// Reads image from PDF
							$imagick->readImage($rutaPdf);
							// Writes an image or image sequence Example- converted-0.jpg, converted-1.jpg
							$imagick->writeImages('/sysx/progs/web/salida/solicitudafiliacion/'.$sRutaAbsoluta.'.tif', false);

						}

						$sRutamostrarpdf = "/sysx/progs/web/salida/solicitudafiliacion/".$iPidActual."SolicitudAfiliacionRegistroH1".$arrDatosSolicitud['curp'].".pdf";
						$arrResp['respuesta'] 	= 1;
						$arrResp['mensaje'] 	= 'Exito';
						$arrResp['rutapdf']	    = $sRutamostrarpdf;
						echo json_encode($arrResp);
						CLogImpresion::escribirLog("Se guarda y muestra PDF de REGISTRO");
					}
				}
				else if( $iFirma == 2 || $iFirma == 0)
				{
					$pdf->Output($rutaPdf, 'I');
					CLogImpresion::escribirLog("Se guarda y muestra PDF de REGISTRO");
					$respuesta->descestatus = 'EXITO';
					$respuesta->estado = 1;
				}

				//SE VALIDA SI ES CON SIGNPAD O SIN FIRMA DEL TRABAJADOR (HUELLA)
				if ($iFirma == 1)
				{
					unset($arrResp);
					
					CLogImpresion::escribirLog("Entra a parte donde se valida si es con Signpad o Sin Firma del trabajador (Huella) [iFirma == 1].");
					
					$arrResp = $objMetodosExpedienteAfiliacion->ctrlImagenesPorTransmitir(3,$iFolio,$sRutaAbsoluta); //iOpcion 3 [SR00,ST00] Solicitud de Afiliacion
					CLogImpresion::escribirLog("Pasaron los datos del PDF a ctrlImagenesPorTransmitir correctamente.");
					
					if($arrResp["estado"] == OK___)
					{
						unset($arrResp);
						//OBTIENE LA CLAVE DE OPERACION
						$arrResp = $objMetodosExpedienteAfiliacion->obtenerclaveoperacion($iFolio, 1);

						if($arrResp["estado"] == OK___)
						{
							$iCveOperacion = $arrResp['respuesta'];
							$respuesta->descestatus = 'EXITO';
							$respuesta->estado = 1;

						}
						else
						{
							CLogImpresion::escribirLog("No se pudo obtener la clave de operacion que le corresponde");
							$respuesta->descestatus = "PROMOTOR: El documento no pudo ser indexado automáticamente. Favor de realizar la captura de firmas para la Impresion de la Solicitud de Registro.";
							$respuesta->estado = -7;
						}
					}
					else
					{
						CLogImpresion::escribirLog("No se pudo actualizar la firma");
						$respuesta->descestatus = "PROMOTOR: El documento no pudo ser indexado automáticamente. Favor de realizar la captura de firmas para la Impresión de Solicitud de Registro.";
						$respuesta->estado = -1;
					}
				}
			}

			catch (Exception $Ex)
			{
				CLogImpresion::escribirLog("Se borran las imagenes del servidor");
				$respuesta->descestatus = $Ex->getMessage();
				$respuesta->estado = -8;
				CLogImpresion::escribirLog($respuesta->descestatus);
			}

			return($respuesta);
}				
function generarSolicitudRegistroHoja2($arrDatosSolicitud, $iFirma, $iFolio)
{
	$iPidActual = getmypid();
	$respuesta = new stdClass();
	$pdf = new PDF_Code39();
	$pdf->AliasNbPages();
	$pdf->AddPage('P','A4');
	$iAltoRen = 5.5;
	$iSaltoLinea = 5.5;
	$sizeFontTitulo = 14;
	$sizeFontCuerpo = 12;
	$sizeFontPiePagina = 6;
	$iRadio = 1;
	$iBordoCel = 0;
	$iBorder = 10;
	$FinMargen = 203;
	$iAltoRen2 = 1;
	$InicioMargen = 4;
	$iAltoRenCabecera = 5.5;
	$iSaltoLineaCabecera = 5;
	$sizeFontCuadro = 11;
	$sizeFontCuerpo2 = 6;
	$SangriaMargen = 5.5;
	$interliniadoCuerpo = 2.6;
	$iSaltoLinea2 = 1;
	$iSaltoLineaExtra=0.3;
	$iSaltoLineaC = 3.5;

	$pdf->SetMargins(4, 4 ,4);

	$pdf->SetFillColor(80,80,80); //Se establece el color de relleno para el encabezado
	$pdf->SetTextColor(255,255,255); //Se establece el color de fuente blanco para el encabezado
	$pdf->SetTextColor(0,0,0);
	       
		   $pdf->SetAutoPageBreak(true,10);
           $pdf->SetFont('Tahoma','',$sizeFontCuerpo2);
           $pdf->setX($SangriaMargen);
           $pdf->MultiCell(($FinMargen+$InicioMargen)-$SangriaMargen, $interliniadoCuerpo, utf8_decode("
III. CONDUSEF, a la Comisión Nacional para la Protección y Defensa de los Usuarios de Servicios Financieros;
IV. CONSAR, a la Comisión Nacional del Sistema de Ahorro para el Retiro;
V. Cuenta Individual, aquella de la que sea titular un trabajador en la cual se depositarán las cuotas obrero patronales y estatales y sus rendimientos, se registrarán las aportaciones a  los  fondos  de vivienda y se depositarán los demás recursos que en términos de la Ley pueden ser aportados;
VI. Empresa Auxiliar, las personas morales que contrate la AFORE, directamente o a través  de  la Empresa Operadora para prestar servicios de ventanilla a los Trabajadores para la recepción de recursos de Ahorro Voluntario;
VII. Empresa Operadora, a la Empresa Concesionaria para operar la Base de Datos Nacional SAR;
VIII. Expediente Electrónico, al conjunto de documentos, datos e información individual, ordenada y detallada que se almacenen en medios digitales o Medios Electrónicos y que permitan la identificación de las personas y de las operaciones y trámites realizados en los Sistemas de Ahorro para el Retiro;
IX. Expediente de Identificación del Trabajador, al conjunto de documentos, datos e información individual de cada Trabajador que permitan su identificación en los Sistemas de Ahorro para el Retiro y que forme parte del Expediente Electrónico.
X. IMSS, al Instituto Mexicano del Seguro Social;
XI. Institutos de Seguridad Social, a los Institutos Mexicanos del Seguro Social, Instituto del Fondo Nacional de la Vivienda para los Trabajadores, Instituto de Seguridad y Servicios Sociales de los Trabajadores del Estado, al Fondo de la Vivienda del ISSSTE;
XII. ISSSTE, al Instituto de Seguridad y Servicios Sociales de los Trabajadores del Estado;
XIII. Ley, a la Ley de los Sistemas de Ahorro para el Retiro;
XIV. Ley de Protección, a la Ley de Protección y Defensa de los  Usuarios de Servicios Financieros;
XV. Leyes de Seguridad Social, a la Ley del Seguro Social 97, Ley del ISSSTE, Ley del INFONAVIT y en su caso, Ley del Seguro Social 73 y Ley del ISSSTE vigente hasta el 31 de marzo de 2007;
XVI. LFPDPPP: Ley Federal de Protección de Datos Personales en Posesión de los Particulares.
XVII. Persona Políticamente Expuesta, aquél individuo que desempeña o ha desempeñado funciones públicas destacadas en un país extranjero o territorio nacional, considerando, entre otros, a los jefes de estado o de gobierno, líderes políticos, funcionarios gubernamentales, judiciales o militares de alta jerarquía, altos ejecutivos de empresas estatales o funcionarios o miembros importantes de partidos políticos. Se asimilan a las Personas políticamente expuestas, el cónyuge y las personas con las que mantenga parentesco por consanguinidad o afinidad hasta el segundo grado, así como los asociados cercanos de la Persona políticamente expuesta.
XVIII. Reglamento, al Reglamento de la Ley de los Sistemas de Ahorro para el Retiro;
XIX. Reglas Generales SAR, a las reglas o disposiciones de carácter general en materia de operaciones expedidas por la CONSAR o por cualquier otra autoridad competente.
XX. SIEFORE(s) o Sociedad(es) de Inversión Especializada(s) de Fondos para el Retiro, aquellas autorizadas por la CONSAR, que opera la AFORE.
XXI. TRABAJADOR, la persona física cuyos datos aparecen en la Solicitud de Registro y/o Traspaso en Administradora de Fondos para el Retiro que forman parte integral del presente contrato.
XXII. Trabajador Independiente, el Trabajador que no tenga una Cuenta Individual abierta en una AFORE, y que no haya estado ni esté sujeto a los regímenes obligatorios previstos en la Ley del Seguro Social, ni en la Ley del ISSSTE.
XXIII. Trabajador ISSSTE, al Trabajador sujeto al régimen obligatorio establecido en la Ley del ISSSTE."), $iBorder, 'J');

	$pdf->Ln($iSaltoLinea2);
    $pdf->SetFont('Tahoma','B',$sizeFontCuerpo2);
    $pdf->setX($SangriaMargen);
    $pdf->Cell( 203, $iAltoRen2, 'SEGUNDA.-  OBJETO DEL CONTRATO ' , $iBorder/**/, 0, 'L');
    $pdf->Ln($iSaltoLinea2+$iSaltoLineaExtra);
    
    $pdf->SetFont('Tahoma','',$sizeFontCuerpo2);
    $pdf->setX($SangriaMargen);
    
    
    $pdf->MultiCell(($FinMargen+$InicioMargen)-$SangriaMargen, $interliniadoCuerpo, utf8_decode("La AFORE se obliga para con el TRABAJADOR a administrar y operar los recursos de  su  Cuenta Individual prevista en la Ley y en las Leyes de Seguridad Social, a prestarle los servicios de compra y venta de acciones de las SIEFORES que la propia AFORE opere, actuando en nombre y por cuenta del TRABAJADOR, así como también los servicios de guarda y administración relativos a tales acciones.
Por su parte, el TRABAJADOR se obliga a pagar, como contraprestación por los referidos servicios las comisiones autorizadas por la CONSAR, que se determinen en la estructura de comisiones de la AFORE."), $iBorder, 'J');

    $pdf->Ln($iSaltoLinea2);
    $pdf->SetFont('Tahoma','B',$sizeFontCuerpo2);
    $pdf->setX($SangriaMargen);
    $pdf->Cell( 203, $iAltoRen2, utf8_decode('TERCERA.- OBLIGACIONES ESPECÍFICAS DE LA AFORE ') , $iBorder/**/, 0, 'L');
    $pdf->Ln($iSaltoLinea2+$iSaltoLineaExtra);
    
    $pdf->SetFont('Tahoma','',$sizeFontCuerpo2);
    $pdf->setX($SangriaMargen);
    $pdf->MultiCell(($FinMargen+$InicioMargen)-$SangriaMargen, $interliniadoCuerpo, utf8_decode("En la prestación de los servicios materia de este contrato, la AFORE tendrá las obligaciones específicas siguientes:
I. Abrir, administrar y operar la Cuenta Individual del Trabajador afiliado al IMSS, del Trabajador ISSSTE o Independiente,  dichas cuentas se sujetarán a las disposiciones de la Ley, su Reglamento,   las  Leyes de Seguridad Social,  sus reglamentos, las Reglas Generales SAR y demás disposiciones aplicables según corresponda en los términos previstos en el artículo 74 de la Ley. Tratándose de  la  subcuenta de vivienda, la AFORE deberá individualizar las aportaciones y rendimientos correspondientes  con  base en la información que le proporcionen los Institutos de Seguridad Social.  La  canalización  de  los  recursos de dicha subcuenta se hará en los términos previstos por la Ley y por las Leyes de Seguridad Social;
II. Recibir las cuotas y aportaciones de seguridad social correspondientes a las Cuentas Individuales de conformidad con las Leyes de Seguridad Social, así como las aportaciones voluntarias, de ahorro a largo plazo y complementarias de retiro, y los demás recursos que en términos de la Ley puedan ser recibidos en las Cuentas Individuales y administrar los recursos de los fondos de previsión social;
III. Individualizar las cuotas y aportaciones destinadas a las Cuentas Individuales, así como los rendimientos derivados de la inversión de las mismas;
IV. Enviar al domicilio o correo electrónico que le indique el TRABAJADOR su certificación de Registro o Traspaso, sus estados de cuenta y proporcionarle información de su Cuenta Individual;
V. Registrar en la Cuenta Individual del TRABAJADOR y en sus respectivas subcuentas, el monto de las cuotas y aportaciones recibidas, los demás recursos que en términos de la Ley puedan ser  recibidos  en la Cuenta Individual, sus rendimientos, así como las comisiones y retiros que se carguen a las mismas.
VI. Prestar al TRABAJADOR los servicios de adquisición y venta de acciones representativas  del  capital de las SIEFORES que opere y que hayan sido elegidas por el TRABAJADOR en las proporciones que expresamente le instruya;
VII. Prestar al TRABAJADOR los servicios de guarda y administración de acciones representativas del capital de las SIEFORES que opere en términos de la cláusula décima quinta del presente contrato;
VIII. Operar y pagar, bajo las modalidades que la CONSAR autorice, los retiros programados del TRABAJADOR;
IX. Pagar los retiros parciales o totales con cargo a la Cuenta Individual del TRABAJADOR  en  los términos de la Ley, su reglamento, las Leyes de Seguridad Social, sus reglamentos y de las Reglas Generales del SAR;
X. Entregar los recursos que correspondan de la Cuenta Individual  a las Instituciones de Seguros que el TRABAJADOR o sus beneficiarios hayan elegido, para la contratación de una renta vitalicia o  de  un seguro de sobrevivencia cuando así corresponda;
XI. Funcionar como entidad financiera autorizada, en términos de lo dispuesto por la Ley del ISSSTE u otros ordenamientos;
XII. Los análogos o conexos a las anteriores que sean autorizados por la Junta de Gobierno de la CONSAR;
XIII. Contar con una Unidad Especializada, en la que atienda las consultas y reclamaciones del TRABAJADOR, de sus beneficiarios en su caso, o de su patrón, así como contar con servicios de información y atención al público. El domicilio de la Unidad Especializada para la Atención de consultas y reclamaciones para el Público, se señala en la parte inferior del anverso de la solicitud de Registro, Traspaso y Recertificación.
XIV. Mantener disponibles los Prospectos de Información y sus folletos explicativos;
XV. Actuar en representación del TRABAJADOR, con su consentimiento, en la atención y resolución de cuestiones relacionadas con su Cuenta Individual;
XVI. Dar a conocer al TRABAJADOR las Empresas Auxiliares en las que podrá realizar los depósitos a los que se refieren las fracciones anteriores.
"), $iBorder+5, 'J');

	$pdf->Ln($iSaltoLinea2);
	$pdf->SetFont('Tahoma','B',$sizeFontCuerpo2);
	$pdf->setX($SangriaMargen);
	$pdf->Cell( 203, $iAltoRen2, utf8_decode('CUARTA.- OBLIGACIONES ESPECÍFICAS DEL TRABAJADOR') , $iBorder /**/, 0, 'L');
	
	$sRutaNombreTrabajador = $arrDatosSolicitud['nombre'].' '.$arrDatosSolicitud['paterno'].' '.$arrDatosSolicitud['materno'];
	$pdf->SetFont('Arial','B', 7.5);
	$pdf->SetTextColor(0,0,0);
	$pdf->setX(70);
	$pdf->Cell( 203, $iAltoRen2, utf8_decode($sRutaNombreTrabajador) , $iBorder /**/, 0, 'L');
	
	$pdf->Ln($iSaltoLinea2+$iSaltoLineaExtra);
	$pdf->SetFont('Tahoma','',$sizeFontCuerpo2);
	$pdf->setX($SangriaMargen);
	$pdf->MultiCell(($FinMargen+$InicioMargen)-$SangriaMargen, $interliniadoCuerpo, utf8_decode("El TRABAJADOR se obliga a lo siguiente:
I. A pagar a la AFORE por los servicios que ésta le preste, las comisiones que la AFORE determine en su estructura correspondiente y que sean autorizadas por la CONSAR en los términos previstos por las disposiciones legales y reglamentarias aplicables;
II.Proporcionar a la AFORE la información que le sea requerida de conformidad con las Reglas Generales SAR, a fin de integrar su expediente de identificación.
III.A informar a la AFORE sobre cualquier modificación de la información consignada en el contrato, usando  los procedimientos que la AFORE ponga a su disposición para tales efectos.
Al respecto el TRABAJADOR se obliga a actualizar, en su caso, los siguientes datos:
a)  Domicilio Particular y/o domicilio donde el TRABAJADOR desea que le sea enviada la correspondencia relacionada con su Cuenta Individual;
b)  El número de teléfono de su domicilio;
c)  Su teléfono celular
d)  Su cuenta de correo electrónico
e)  Cambio de beneficiarios sustitutos;
La actualización de la información deberá comunicarse a la AFORE, a más tardar dentro de  los  treinta días naturales siguientes a la realización de los hechos que dieron origen a la modificación, de acuerdo a los procedimientos que la AFORE ponga para tales efectos."), $iBorder, 'J');
	$pdf->Ln($iSaltoLinea2);
	$pdf->SetFont('Tahoma','B',$sizeFontCuerpo2);
	$pdf->setX($SangriaMargen);
	$pdf->Cell( 203, $iAltoRen2, utf8_decode('QUINTA.- OTORGAMIENTO DE LA COMISIÓN MERCANTIL POR PARTE DEL TRABAJADOR A LA ADMINISTRADORA') , $iBorder/**/, 0, 'L');
//	$pdf->Ln($iSaltoLinea2 + 1);
//	$pdf->setX($SangriaMargen);
//	$pdf->Cell( 203, $iAltoRen2, utf8_decode('LA ADMINISTRADORA') , $iBorder/**/, 0, 'L');
	$pdf->Ln($iSaltoLinea2+$iSaltoLineaExtra);

	$pdf->SetFont('Tahoma','',$sizeFontCuerpo2);
	$pdf->setX($SangriaMargen);
	$pdf->MultiCell(($FinMargen+$InicioMargen)-$SangriaMargen, $interliniadoCuerpo, utf8_decode("El TRABAJADOR otorga a la AFORE, en este acto y por medio del presente contrato, Comisión Mercantil, para que ésta, por su cuenta y orden, adquiera, enajene, mantenga en custodia y administre acciones representativas del capital social de las SIEFORES operadas por la AFORE, relacionadas con la Cuenta Individual del TRABAJADOR.
Dentro de la Comisión Mercantil que el TRABAJADOR confiere en los términos de este contrato, también se comprende de manera específica la facultad de hacerse representar por la AFORE  en  las asambleas de las SIEFORES de las que sea accionista, respecto de las acciones que son objeto del  servicio  de guarda y administración.
Esta facultad se ejercitará por la AFORE cuando el TRABAJADOR no manifieste por escrito su interés en asistir a una asamblea cuando menos con ocho días hábiles de anticipación a la celebración de la misma. La AFORE informará al TRABAJADOR, cuando éste así lo solicite por escrito, acerca de los acuerdos adoptados en las asambleas a las que hubiere concurrido en su representación.
La ejecución de la Comisión Mercantil se apegará en todo momento a la Ley, las Leyes de Seguridad Social, al Reglamento y al presente contrato, así como a las demás disposiciones que le sean aplicables."), $iBorder, 'J');

	$pdf->Ln($iSaltoLinea2);
	$pdf->SetFont('Tahoma','B',$sizeFontCuerpo2);
	$pdf->setX($SangriaMargen);
	$pdf->Cell( 203, $iAltoRen2, utf8_decode('SEXTA.- INSTRUCCIONES DEL TRABAJADOR A LA ADMINISTRADORA') , $iBorder/**/, 0, 'L');
	$pdf->Ln($iSaltoLinea2+$iSaltoLineaExtra);

	$pdf->SetFont('Tahoma','',$sizeFontCuerpo2);
	$pdf->setX($SangriaMargen);
	$pdf->MultiCell(($FinMargen+$InicioMargen)-$SangriaMargen, $interliniadoCuerpo, utf8_decode("Las partes convienen en que las instrucciones que el TRABAJADOR otorgue a la AFORE respecto de las operaciones y servicios materia de este contrato, podrán ser escritas, o utilizando al efecto medios electrónicos, ópticos, de cómputo, de telecomunicaciones en general, medios biométricos o de cualquier otra tecnología (incluyendo de manera ejemplificativa más no limitativa, sistemas automatizados, de transmisión o de procesamiento de datos, telefonía en todas sus modalidades, redes de telecomunicaciones, Internet, correo electrónico, módems, mini terminales portátiles, cajeros automáticos, sign pad, tabletas electrónicas, etc.), en lo sucesivo referidos como MEDIOS INFORMÁTICOS.  El uso de MEDIOS INFORMÁTICOS y/o biométricos, en sustitución de la firma autógrafa, producirá los mismos efectos legales como si hubiera manifestado la voluntad por  escrito  y, en consecuencia, tendrá el mismo valor probatorio, según lo dispuesto por la Ley y el  Código  Civil Federal.
La AFORE proporcionará al TRABAJADOR diversas claves de acceso, de identificación y en su caso de operación, sin perjuicio de cualquier otra que de  tiempo en tiempo sea acordada entre las partes (las CLAVES DE IDENTIFICACIÓN RECÍPROCA).  Mismas que al ser digitadas sustituirán a la firma autógrafa por una firma electrónica (entendiendo por tal al conjunto de datos que se agregan o adjuntan a un mensaje de datos  al  cual  está asociado  en  forma  lógica a ésta  y es atribuible al TRABAJADOR) y con ello podrá girar instrucciones a la AFORE y/o usar los servicios a que se refiere este contrato mediante la utilización de medios Informáticos y/o biométricos."), $iBorder, 'J');

	$pdf->Ln($iSaltoLinea2);
	$pdf->SetFont('Tahoma','B',$sizeFontCuerpo2);
	$pdf->setX($SangriaMargen);
	$pdf->Cell( 203, $iAltoRen2, utf8_decode('SÉPTIMA.- TÉRMINOS EN QUE SE PONDRÁN A DISPOSICIÓN DEL TRABAJADOR LOS PROSPECTOS DE INFORMACIÓN') , $iBorder/**/, 0, 'L');
//	$pdf->Ln($iSaltoLinea2 + 1);
//	$pdf->setX($SangriaMargen);
//	$pdf->Cell( 203, $iAltoRen2, utf8_decode('PROSPECTOS DE INFORMACIÓN') , $iBorder/**/, 0, 'L');
	$pdf->Ln($iSaltoLinea2+$iSaltoLineaExtra);

	$pdf->SetFont('Tahoma','',$sizeFontCuerpo2);
	$pdf->setX($SangriaMargen);

	$pdf->MultiCell(($FinMargen+$InicioMargen)-$SangriaMargen, $interliniadoCuerpo, utf8_decode("Las partes convienen en que para la elección de una o más SIEFORES de las que opere la AFORE, el TRABAJADOR tendrá  a su disposición el o los Prospectos de Información y sus folletos explicativos en la página web de la Administradora www.aforecoppel.com,  así como en la Unidad Especializada  de  la Afore."), $iBorder, 'J');

	$pdf->Ln($iSaltoLinea2);
	$pdf->SetFont('Tahoma','B',$sizeFontCuerpo2);
	$pdf->setX($SangriaMargen);
	$pdf->Cell( 203, $iAltoRen2, utf8_decode('OCTAVA.- TRASPASO DE RECURSOS ENTRE SIEFORES') , $iBorder/**/, 0, 'L');
	$pdf->Ln($iSaltoLinea2+$iSaltoLineaExtra);

	$pdf->SetFont('Tahoma','',$sizeFontCuerpo2);
	$pdf->setX($SangriaMargen);

	$pdf->MultiCell(($FinMargen+$InicioMargen)-$SangriaMargen, $interliniadoCuerpo, utf8_decode("El TRABAJADOR podrá solicitar en cualquier tiempo la transferencia de los  recursos  de su Cuenta Individual de una Sociedad de Inversión Básica a otra de conformidad con lo establecido en las disposiciones emitidas por la CONSAR para este fin, siempre y cuando el régimen de Inversión de la Siefore lo permita. "), $iBorder, 'J');

	$pdf->Ln($iSaltoLinea2);
	$pdf->SetFont('Tahoma','B',$sizeFontCuerpo2);
	$pdf->setX($SangriaMargen);
	$pdf->Cell( 203, $iAltoRen2, utf8_decode('NOVENA.- TRASPASO DE LA CUENTA INDIVIDUAL A OTRA ADMINISTRADORA') , $iBorder/**/, 0, 'L');
	$pdf->Ln($iSaltoLinea2+$iSaltoLineaExtra);

	$pdf->SetFont('Tahoma','',$sizeFontCuerpo2);
	$pdf->setX($SangriaMargen);

	$pdf->MultiCell(($FinMargen+$InicioMargen)-$SangriaMargen, $interliniadoCuerpo, utf8_decode("El TRABAJADOR podrá traspasar su Cuenta Individual a otra Administradora, una vez que transcurra un año, contado a partir de que el TRABAJADOR se registró o de la última ocasión en  que  haya ejercitado su derecho.
Para ejercer este derecho el TRABAJADOR deberá presentar su solicitud de Traspaso a la Administradora que haya elegido quien será la responsable del seguimiento y de efectuar los trámites correspondientes para el Traspaso.
"), $iBorder, 'J');

    $rutaPdf = "/sysx/progs/web/salida/solicitudafiliacion/".$iPidActual."SolicitudAfiliacionRegistroH2".$arrDatosSolicitud['curp'].".pdf";
    $respuesta->rutaPdf = "/sysx/progs/web/salida/solicitudafiliacion/".$iPidActual."SolicitudAfiliacionRegistroH2".$arrDatosSolicitud['curp'].".pdf";
    $sRutaAbsoluta = $iPidActual."SolicitudAfiliacionRegistroH2".$arrDatosSolicitud['curp'].".pdf";

    //ARCHIVO TEMPORAL
    $rutaPdfTemp = "/sysx/progs/web/salida/solicitudafiliacion/".$arrDatosSolicitud['folio']."_".$iPidActual."SolicitudAfiliacionRegistroH2".$arrDatosSolicitud['curp']."_TEMP.pdf";

			//RUTA DE LAS FIRMAS
    $sRutaFirmaTrabajador = "/sysx/progs/web/entrada/firmas/UFAF_".$arrDatosSolicitud['folio']."_FTRAB.JPG";
    $sRutaFirmaPromotor = "/sysx/progs/web/entrada/firmas/UFAF_".$arrDatosSolicitud['folio']."_FPROM.JPG";

			try
			{
				/*$sRutaNombreTrabajador = $arrDatosSolicitud['nombre'].' '.$arrDatosSolicitud['paterno'].' '.$arrDatosSolicitud['materno'];
				//$pdf->Image('/sysx/progs/web/entrada/firmas/ST00_'.$arrDatosSolicitud["folio"].'_NTRAB.JPG', 113, 213.5, 70, 12, 'jpg');
				$pdf->SetFont('Arial','B', 7.5);
				$pdf->SetTextColor(0,0,0);
				$pdf->SetXY(112, 149.1);
				$pdf->Write(15, $sRutaNombreTrabajador);*/
				$pdf->Output($rutaPdf);
				$objMetodosExpedienteAfiliacion = new CMetodosExpedienteAfiliacion();


				if ($iFirma == 1)
				{
						$pdf = new FPDI();

						$pageCount = $pdf->setSourceFile('/sysx/progs/web/salida/solicitudafiliacion/'.$sRutaAbsoluta);
						$tplIdx = $pdf->importPage(1);
						$pdf->addPage();
						$pdf->useTemplate($tplIdx, null, null, 0, 0, true);
						$arrResp = $objMetodosExpedienteAfiliacion->ctrlImagenesPorTransmitir(3,$iFolio,$sRutaAbsoluta); //iOpcion 3 [SR00,ST00] Solicitud de Afiliacion
						CLogImpresion::escribirLog("Pasaron los datos del PDF a ctrlImagenesPorTransmitir correctamente.");
						$user_agent = $_SERVER['HTTP_USER_AGENT'];
						$OSname = getPlatform($user_agent);
						if($OSname == "Android"){

							// Envio de correo
							$sDocumento = $sRutaAbsoluta;
							$sUbicacion = "/sysx/progs/web/salida/solicitudafiliacion/";
							$sIpOrigen = $_SERVER["HTTP_HOST"];
							$sTipo = "SR00";

							$arrAPI = array(
								'folio' => $arrDatosSolicitud['folio'],
								'documento' => $sDocumento,
								'ubicacion' => $sUbicacion,
								'iporigen' => $sIpOrigen,
								'tipodocumento' => $sTipo
							);

						}


                    $iContinuar = 1;
					if($iContinuar == 1)
					{
						$pdf->Output($rutaPdf, 'F');
						copy($rutaPdf, $rutaPdfTemp);
						chmod($rutaPdf, 0777);
						
                  
				     $pdf = new FPDI();
				     for ($i = 1; $i <= $pageCount; $i++)
			     	 {
					   $pageCount = $pdf->setSourceFile('/sysx/progs/web/salida/solicitudafiliacion/'.$sRutaAbsoluta);
					   $tplIdx = $pdf->importPage($i);
					   $pdf->addPage();
					   $pdf->useTemplate($tplIdx, null, null, 0, 0, true);
				     }

                        //SE GUARDA PDF
				        $pdf->Output($rutaPdf, 'F');
				        $pdf = new FPDI();
				        //SE AGREGA ESTE "FOR" PARA QUE SE MODIFIQUE SOLAMENTE LA PRIMER PAGINA DEL PDF PERO AL GUARDAR SE GUARDEN TODAS LAS PAGINAS
				        for ($i = 1; $i <= $pageCount; $i++)
			         	{
					      $pageCount = $pdf->setSourceFile('/sysx/progs/web/salida/solicitudafiliacion/'.$sRutaAbsoluta);
					      $tplidx = $pdf->ImportPage($i);
					      $pdf->addPage();
					      $pdf->useTemplate($tplidx, null, null, 0, 0, true);
				        }
    
						//copy($rutaPdf, $rutaPdfTemp);

					     $pdf = new FPDI();
				        for ($i = 1; $i <= $pageCount; $i++)
				        {
				        	$pageCount = $pdf->setSourceFile('/sysx/progs/web/salida/solicitudafiliacion/'.$sRutaAbsoluta);
				        	$tplidx = $pdf->ImportPage($i);
				        	$pdf->addPage();
				        	$pdf->useTemplate($tplIdx, null, null, 0, 0, true);
				        }
				        //SE GUARDA PDF
				
						$pdf->Output($rutaPdf, 'F');

						$user_agent = $_SERVER['HTTP_USER_AGENT'];
						$OSname = getPlatform($user_agent);
						if($OSname == "Android"){

							// create Imagick object
							$imagick = new Imagick();
							// Reads image from PDF
							$imagick->setResolution(500, 500);
							$imagick->readImage($rutaPdf);

							$sRuta_imag = str_replace(".pdf","",$sRutaAbsoluta);

							// Writes an image or image sequence Example- converted-0.jpg, converted-1.jpg
							$imagick->writeImages('/sysx/progs/web/salida/solicitudafiliacion/'.$sRuta_imag.'.tif', false);

						

						}

						$sRutamostrarpdf = "/sysx/progs/web/salida/solicitudafiliacion/".$iPidActual."SolicitudAfiliacionRegistroH2".$arrDatosSolicitud['curp'].".pdf";
						$arrResp['respuesta'] 	= 1;
						$arrResp['mensaje'] 	= 'Exito';
						$arrResp['rutapdf']	    = $sRutamostrarpdf;
						echo json_encode($arrResp);
						CLogImpresion::escribirLog("Se guarda y muestra PDF de REGISTRO");
				
				    }
			     }	
             }
				
			catch (Exception $Ex)
			{
				CLogImpresion::escribirLog("Se borran las imagenes del servidor");
				$respuesta->descestatus = $Ex->getMessage();
				$respuesta->estado = -8;
				CLogImpresion::escribirLog($respuesta->descestatus);
			}

			return($respuesta);
}			
function generarSolicitudRegistroHoja3($arrDatosSolicitud, $iFirma, $iFolio)
{
	$iPidActual = getmypid();
	$respuesta = new stdClass();
	$pdf = new PDF_Code39();
	$pdf->AliasNbPages();
	$pdf->AddPage('P','A4');
	$iAltoRen = 5.5;
	$iSaltoLinea = 5.5;
	$sizeFontTitulo = 14;
	$sizeFontCuerpo = 12;
	$sizeFontPiePagina = 6;
	$iRadio = 1;
	$iBordoCel = 0;
	$iBorder = 10;
	$FinMargen = 203;
	$iAltoRen2 = 1;
	$InicioMargen = 4;
	$iAltoRenCabecera = 5.5;
	$iSaltoLineaCabecera = 5;
	$sizeFontCuadro = 11;
	$sizeFontCuerpo2 = 6;
	$SangriaMargen = 5.5;
	$interliniadoCuerpo = 2.6;
	$iSaltoLinea2 = 1;
	$iSaltoLineaExtra=0.3;
	$iSaltoLineaC = 3.5;

	$pdf->SetMargins(4, 4 ,4);

	$pdf->SetFillColor(80,80,80); //Se establece el color de relleno para el encabezado
	$pdf->SetTextColor(255,255,255); //Se establece el color de fuente blanco para el encabezado
	$pdf->SetTextColor(0,0,0);
 
            $pdf->SetFont('Tahoma','',$sizeFontCuerpo2);
            $pdf->setX($SangriaMargen);
            $pdf->MultiCell(($FinMargen+$InicioMargen)-$SangriaMargen, $interliniadoCuerpo, utf8_decode("Asimismo el TRABAJADOR podrá traspasar su Cuenta Individual antes de cumplir un año cuando se encuentren en alguno de los supuestos previstos en el artículo 74 de la Ley  apegándose  en lo conducente a los plazos previstos en la cláusula VIGÉSIMA PRIMERA SEGUNDA del presente contrato."), $iBorder, 'J');
			
			$pdf->Ln($iSaltoLinea2);
			$pdf->SetFont('Tahoma','B',$sizeFontCuerpo2);
			$pdf->setX($SangriaMargen);
			$pdf->Cell( 203, $iAltoRen2, utf8_decode('DÉCIMA.- MANEJO Y REGISTRO DE LAS SUBCUENTAS DE VIVIENDA') , $iBorder/**/, 0, 'L');
			$pdf->Ln($iSaltoLinea2+$iSaltoLineaExtra);

			$pdf->SetFont('Tahoma','',$sizeFontCuerpo2);
			$pdf->setX($SangriaMargen);

			$pdf->MultiCell(($FinMargen+$InicioMargen)-$SangriaMargen, $interliniadoCuerpo, utf8_decode("En el caso de las aportaciones que sean realizadas a la subcuenta de vivienda, la administración de los recursos estará a cargo del Instituto del Fondo Nacional de la Vivienda para los Trabajadores o del FOVISSSTE según corresponda, por lo que la AFORE únicamente individualizará y registrará  la información relativa a los importes de las aportaciones que se realicen a la subcuenta de vivienda cuyos recursos se canalizarán en los términos previstos por las Leyes de Seguridad. El TRABAJADOR recibirá intereses sobre los importes correspondientes, en los términos previstos por la  Ley  del Instituto del Fondo Nacional de la Vivienda para los Trabajadores y/o la Ley de FOVISSSTE"), $iBorder, 'J');

			$pdf->Ln($iSaltoLinea2);
			$pdf->SetFont('Tahoma','B',$sizeFontCuerpo2);
			$pdf->setX($SangriaMargen);
			$pdf->Cell( 203, $iAltoRen2, utf8_decode('DÉCIMA PRIMERA.- ADMINISTRACIÓN DE LAS CUENTAS INDIVIDUALES SAR ANTERIORES AL 1º DE JULIO DE 1997 Y MANEJO DE INFORMACIÓN SAR') , $iBorder/**/, 0, 'L');
//			$pdf->Ln($iSaltoLinea2 + 1);
//			$pdf->setX($SangriaMargen);
//			$pdf->Cell( 203, $iAltoRen2, utf8_decode('AL 1º DE JULIO DE 1997 Y MANEJO DE INFORMACIÓN SAR') , $iBorder/**/, 0, 'L');
			$pdf->Ln($iSaltoLinea2+$iSaltoLineaExtra);

			$pdf->SetFont('Tahoma','',$sizeFontCuerpo2);
			$pdf->setX($SangriaMargen);

			$pdf->MultiCell(($FinMargen+$InicioMargen)-$SangriaMargen, $interliniadoCuerpo, utf8_decode("El TRABAJADOR  otorga expresamente su consentimiento para que la AFORE realice por su cuenta y orden los trámites necesarios para que los saldos de su cuenta o cuentas individuales abiertas en Instituciones de Crédito con anterioridad al 1° de julio de 1997 así como de la subcuenta de ahorro para el retiro abierta conforme a la ley del ISSSTE sean transferidos a la AFORE para que esta última las administre de conformidad con lo previsto por la Ley y las Leyes de Seguridad Social.  A efecto de lo anterior, el TRABAJADOR deberá proporcionar los datos de las Instituciones de Crédito en que tiene abiertas cuentas individuales, así como el número de las mismas, entregando un comprobante relativo a cada una de dichas cuentas.
De igual forma, otorga su consentimiento expreso para que la AFORE proporcione a las Empresas Auxiliares y a la Empresa Operadora de la Base de Datos Nacional SAR, la información de su Cuenta Individual que proceda, en los términos de la Ley, las Leyes de Seguridad Social, el Reglamento y demás disposiciones aplicables.
"), $iBorder, 'J');

			$pdf->Ln($iSaltoLinea2);
			$pdf->SetFont('Tahoma','B',$sizeFontCuerpo2);
			$pdf->setX($SangriaMargen);
			$pdf->Cell( 203, $iAltoRen2, utf8_decode('DÉCIMA SEGUNDA.- RECEPCIÓN  Y RETIRO DE APORTACIONES DE AHORRO VOLUNTARIO.') , $iBorder/**/, 0, 'L');
			$pdf->Ln($iSaltoLinea2+$iSaltoLineaExtra);

			$pdf->SetFont('Tahoma','',$sizeFontCuerpo2);
			$pdf->setX($SangriaMargen);


			$pdf->MultiCell(($FinMargen+$InicioMargen)-$SangriaMargen, $interliniadoCuerpo, utf8_decode("Las aportaciones voluntarias y complementarias de retiro, o de ahorro a largo plazo destinadas a la Cuenta Individual del TRABAJADOR podrán ser realizadas de manera directa por éste o a través de su patrón, en las empresas auxiliares que la AFORE designe.
En ningún caso los agentes promotores de la AFORE podrán recibir recursos por concepto de aportaciones voluntarias y/o complementarias de retiro ni de ahorro a largo plazo.
Las aportaciones voluntarias no se utilizarán para financiar la pensión del TRABAJADOR, a menos que conste su consentimiento expreso para ello, de conformidad con la normativa aplicable.
El TRABAJADOR podrá realizar retiros de su subcuenta de aportaciones voluntarias dentro del plazo que se establezca en el Prospecto de Información de cada SIEFORE que opere la AFORE.
Para realizar retiros con cargo a la subcuenta de aportaciones voluntarias, el TRABAJADOR deberá apegarse a los  procedimientos que la AFORE establezca.
Previo consentimiento del TRABAJADOR, el importe de la subcuenta de aportaciones voluntarias podrá transferirse a la  subcuenta de vivienda para su aplicación en un crédito de vivienda otorgado a su favor por el Instituto  del Fondo Nacional para la Vivienda de los Trabajadores. Esta transferencia podrá realizarse en cualquier momento aún cuando no haya transcurrido el plazo mínimo para disponer de las aportaciones voluntarias en los términos de las Reglas Generales SAR y deberán recibirlas por el mencionado Instituto conforme al procedimiento que al efecto se establezca.
En caso de fallecimiento del TRABAJADOR, tendrán derecho a disponer de las aportaciones voluntarias y complementarias de retiro y de ahorro a largo plazo depositadas en su Cuenta  Individual,  los beneficiarios legales del TRABAJADOR y, a la falta de éstos, las personas que él haya designado para tal efecto, de acuerdo a los procedimientos establecidos por la AFORE.
El TRABAJADOR o sus beneficiarios que hayan obtenido una resolución de otorgamiento de pensión o bien, de negativa de pensión, o que por cualquier otra causa tengan el derecho a retirar la totalidad de los recursos  de su Cuenta Individual, podrán optar por que las  cantidades  depositadas en su subcuentas de ahorro voluntario permanezcan invertidas en las SIEFORES operadas por la AFORE, durante el plazo que consideren conveniente.
Las aportaciones complementarias de retiro solo podrán retirarse cuando el TRABAJADOR tenga derecho a disponer de las aportaciones obligatorias, ya sea para complementar, cuando así lo solicita el TRABAJADOR, los recursos destinados al pago de su pensión, o bien para recibirlos en una sola exhibición.
"), $iBorder, 'J');

			$pdf->SetFont('Tahoma','B',$sizeFontCuerpo2);
			$pdf->setX($SangriaMargen);
			$pdf->MultiCell(($FinMargen+$InicioMargen)-$SangriaMargen,$interliniadoCuerpo, utf8_decode("DÉCIMA TERCERA.- INFORMACIÓN  SOBRE  LA  CUENTA  INDIVIDUAL;  LA  SITUACIÓN  DEL SAR Y LAS PERSPECTIVAS PENSIONARIAS DE LOS TRABAJADORES, Y TÉRMINOS EN LOS QUE ÉSTA SE PONDRÁ A DISPOSICIÓN DE LOS TRABAJADORES, DE CONFORMIDAD CON LO PREVISTO EN LA NORMATIVA APLICABLE
"), 0, 'J');

			$pdf->SetFont('Tahoma','',$sizeFontCuerpo2);
			$pdf->setX($SangriaMargen);
			

				$pdf->MultiCell(($FinMargen+$InicioMargen)-$SangriaMargen, $interliniadoCuerpo, utf8_decode("De conformidad con el artículo 18  fracción IV de la Ley de los Sistemas de Ahorro para el Retiro el TRABAJADOR tiene derecho a recibir de la AFORE su estado de cuenta  y demás información sobre su Cuenta Individual por lo menos tres veces al año, al domicilio que haya registrado el TRABAJADOR, de conformidad con los lineamientos que establezca la CONSAR,  así mismo podrá solicitar a la AFORE su estado de cuenta y demás información sobre su Cuenta Individual en términos de las disposiciones de carácter general en materia de operaciones de los Sistemas de Ahorro para el Retiro y de las políticas y procedimientos establecidos por la AFORE.
La AFORE podrá suspender el envío de los estados de cuenta cuando la dirección proporcionada por el TRABAJADOR no exista o éste no tenga su domicilio en el lugar indicado. En ambos casos, la AFORE conservará el estado de cuenta a disposición del TRABAJADOR, el cual se le entregará sin costo alguno.
La AFORE está obligada a remitir al domicilio del TRABAJADOR un estado de cuenta final de la Cuenta Individual que haya sido traspasada  a otra Administradora.
El TRABAJADOR podrá requerir en cualquier tiempo a la AFORE,  información  sobre el estado de su Cuenta Individual, de conformidad con lo previsto en el artículo 33 fracción IV del Reglamento y a las disposiciones de carácter general emitidas por la CONSAR.
"), $iBorder, 'J');
			$pdf->Ln($iSaltoLinea2);
			$pdf->SetFont('Tahoma','B',$sizeFontCuerpo2);
			$pdf->setX($SangriaMargen);
			$pdf->Cell( 203, $iAltoRen2, utf8_decode('DÉCIMA CUARTA.- DESIGNACIÓN DE BENEFICIARIOS SUSTITUTOS') , $iBorder/**/, 0, 'L');
			$pdf->Ln($iSaltoLinea2+$iSaltoLineaExtra);

			$pdf->SetFont('Tahoma','',$sizeFontCuerpo2);
			$pdf->setX($SangriaMargen);

			$pdf->MultiCell(($FinMargen+$InicioMargen)-$SangriaMargen,$interliniadoCuerpo, utf8_decode("El TRABAJADOR deberá designar a los beneficiarios sustitutos para el caso de que faltaren los beneficiarios legales previstos en la Ley del Seguro Social, publicada el 21 de diciembre de 1995 en el Diario Oficial de la Federación, o bien para el caso de las aportaciones de ahorro a largo plazo, de conformidad con la Ley del ISSSTE, publicada el 27 de diciembre de 1983 en el Diario Oficial de la Federación, ambas regulaciones con sus reformas y adiciones,  precisando el porcentaje de los recursos acumulados en la Cuenta Individual que le corresponderá a cada uno de ellos. A falta de este señalamiento, los recursos se asignarán por partes iguales entre todos los beneficiarios sustitutos designados.
"), $iBorder, 'J');

			$pdf->SetFont('Tahoma','B',$sizeFontCuerpo2);
			$pdf->setX($SangriaMargen);
			$pdf->MultiCell(($FinMargen+$InicioMargen)-$SangriaMargen,$interliniadoCuerpo, utf8_decode("DÉCIMA QUINTA.- SERVICIOS DE GUARDA Y ADMINISTRACIÓN DE ACCIONES REPRESENTATIVAS DEL CAPITAL SOCIAL DE LAS SOCIEDADES DE INVERSIÓN
"), $iBorder, 'J');

			$pdf->SetFont('Tahoma','',$sizeFontCuerpo2);
			$pdf->setX($SangriaMargen);

			$pdf->MultiCell(($FinMargen+$InicioMargen)-$SangriaMargen,$interliniadoCuerpo, utf8_decode("La AFORE prestará al TRABAJADOR los servicios de guarda y administración de las acciones representativas del capital social de las SIEFORES, para lo cual la AFORE deberá mantener depositados dichos títulos en una institución para el depósito de valores concesionada  de  acuerdo con el artículo 55 de la Ley del Mercado de Valores.
La AFORE se obliga a efectuar en relación con dichas acciones, los actos necesarios para la conservación de los derechos que  los referidos títulos confieran o impongan al TRABAJADOR, sin que dentro de éstos se comprenda el ejercicio de derecho o acciones judiciales.
"), $iBorder, 'J');

			$pdf->Ln($iSaltoLinea2);
			$pdf->SetFont('Tahoma','B',$sizeFontCuerpo2);
			$pdf->setX($SangriaMargen);
			$pdf->Cell( 203, $iAltoRen2, utf8_decode('DÉCIMA SEXTA.- EJERCICIO DE DERECHOS PATRIMONIALES') , $iBorder/**/, 0, 'L');
			$pdf->Ln($iSaltoLinea2+$iSaltoLineaExtra);

			$pdf->SetFont('Tahoma','',$sizeFontCuerpo2);
			$pdf->setX($SangriaMargen);

			$pdf->MultiCell(($FinMargen+$InicioMargen)-$SangriaMargen,$interliniadoCuerpo, utf8_decode("Cuando haya que ejercer derechos o efectuar exhibiciones de cualquier clase con relación con las acciones respecto de las cuales la AFORE esté prestando el servicio de guarda  y  administración, se estará a lo siguiente:
a)  Si las acciones atribuyen un derecho de ocupación o preferencia, la AFORE ejercerá tal derecho de acuerdo a las instrucciones que haya recibido del TRABAJADOR, siempre y cuando haya sido provista de los recursos suficientes por lo menos con dos días hábiles antes del vencimiento del plazo señalado para efectuar el pago del derecho opcional o de preferencia;
b)  Los derechos patrimoniales correspondientes a las acciones serán ejercidos por la AFORE por cuenta del TRABAJADOR y acreditados a éste en la subcuenta de aportaciones voluntarias, para ser aplicadas a la compra de la o las acciones representativas del capital social de la SIEFORE correspondiente; y
c)  La falta de entrega por parte del TRABAJADOR de los fondos señalados en los incisos anteriores eximirá a la AFORE de cualquier responsabilidad por la inejecución de los actos mencionados.
"), $iBorder, 'J');

			$pdf->SetFont('Tahoma','B',$sizeFontCuerpo2);
			$pdf->setX($SangriaMargen);
			$pdf->MultiCell(($FinMargen+$InicioMargen)-$SangriaMargen,$interliniadoCuerpo, utf8_decode("DÉCIMA SÉPTIMA.- ESTRUCTURA Y  COBRO  DE  COMISIONES POR LOS SERVICIOS PRESTADOS POR LA ADMINISTRADORA
"), 0, 'J');

			$pdf->SetFont('Tahoma','',$sizeFontCuerpo2);
			$pdf->setX($SangriaMargen);

			$pdf->MultiCell(($FinMargen+$InicioMargen)-$SangriaMargen,$interliniadoCuerpo, utf8_decode("La AFORE cobrará al TRABAJADOR por los servicios que le preste, las comisiones que correspondan de conformidad con la estructura de comisiones que apruebe la CONSAR.
En caso de modificación de la estructura de comisiones, la AFORE, previamente y en la forma y términos que determine la CONSAR, informará al TRABAJADOR la modificación de que se trate.
Las Comisiones serán percibidas  por la AFORE en los términos previstos por la Ley, su Reglamento y las Reglas Generales SAR.
En ningún caso la AFORE podrá cobrar comisiones por entregar los recursos a la institución de seguros que el TRABAJADOR o sus beneficiarios hayan elegido para la contratación de rentas vitalicias o del seguro de sobrevivencia.
"), $iBorder, 'J');
			$pdf->Ln($iSaltoLinea2);
			$pdf->SetFont('Tahoma','B',$sizeFontCuerpo2);
			$pdf->setX($SangriaMargen);
			$pdf->Cell( 203, $iAltoRen2, utf8_decode('DÉCIMA OCTAVA.- RECOMPRA DE ACCIONES') , $iBorder/**/, 0, 'L');
			$pdf->Ln($iSaltoLinea2+$iSaltoLineaExtra);

			$pdf->SetFont('Tahoma','',$sizeFontCuerpo2);
			$pdf->setX($SangriaMargen);

			$pdf->MultiCell(($FinMargen+$InicioMargen)-$SangriaMargen,$interliniadoCuerpo, utf8_decode("El TRABAJADOR tendrá derecho a que la AFORE le recompre al precio de valuación vigente el día que se lleven a cabo las operaciones correspondientes, hasta el cien por ciento de su tenencia de acciones de la SIEFORE en que se encuentren invertidos sus recursos, en los siguientes casos:
a)  Cuando tenga derecho a gozar de una pensión o a alguna otra prestación en los términos  de las Leyes del Seguridad Social;
b)  Cuando se presente una modificación al régimen de inversión de la SIEFORE o a las comisiones que cobre la AFORE;
c)  Cuando solicite el Traspaso de su Cuenta Individual en los plazos que la CONSAR establezca;
d)  Cuando solicite el Traspaso de su Cuenta Individual asignada a otra Administradora;
e)  Cuando solicite el retiro de recursos de su subcuenta de aportaciones voluntarias; y
f)  Cuando la AFORE entre en estado de disolución y liquidación, en el orden de prelación que le corresponda como accionista de la SIEFORE respectiva.
La AFORE deberá efectuar la compra de las acciones en un plazo  máximo  de 30 días naturales contando a partir de que se determine la procedencia de la solicitud presentada por  el  TRABAJADOR, con excepción del supuesto a que se refiere el inciso f) en que se estará al plazo previsto en la Ley y en el Reglamento. El TRABAJADOR podrá, por una sola vez, autorizar que se prorrogue el plazo primeramente citado por otros 30 días naturales.
"), $iBorder, 'J');
			$pdf->Ln($iSaltoLinea2);
			$pdf->SetFont('Tahoma','B',$sizeFontCuerpo2);
			$pdf->setX($SangriaMargen);
			$pdf->Cell( 203, $iAltoRen2, utf8_decode('DÉCIMA NOVENA.- RETIRO DE FONDOS') , $iBorder/**/, 0, 'L');
			$pdf->Ln($iSaltoLinea2+$iSaltoLineaExtra);

			$pdf->SetFont('Tahoma','',$sizeFontCuerpo2);
			$pdf->setX($SangriaMargen);

			$pdf->MultiCell(($FinMargen+$InicioMargen)-$SangriaMargen,$interliniadoCuerpo, utf8_decode("El TRABAJADOR o sus beneficiarios, en caso del fallecimiento o incapacidad total de éste, podrá solicitar el retiro total o parcial de sus recursos en los supuestos de incapacidad permanente, invalidez permanente, muerte, cesantía en edad avanzada, vejez, matrimonio, desempleo  y  demás previstos en las Leyes de Seguridad Social, para lo cual deberá presentar, de manera directa o a través de la Administradora, la solicitud correspondiente ante los Institutos de Seguridad Social a efecto de que emitan una resolución sobre la procedencia del retiro.
En caso de resolución favorable de los Institutos de Seguridad Social correspondientes, la AFORE, una vez que reciba dicha resolución, llevará a cabo la liquidación de los fondos que procedan y a enterarlos en los términos de las Leyes de Seguridad Social, y de las Disposiciones emitidas por la CONSAR aplicables.
Tratándose de los recursos de la subcuenta aportaciones voluntarias el TRABAJADOR podrá solicitar el retiro total o parcial de sus recursos de manera remota siempre y cuando éste último haya integrado ante la Administradora de Fondos para el Retiro y en la Base de Datos Nacional SAR su Expediente de Identificación contemplado por la normativa aplicable, que permita la cabal identificación del TRABAJADOR, de lo contrario deberá acudir al módulo de servicio de su preferencia a efecto de integrar el Expediente de Identificación correspondiente.  En el caso de sus beneficiarios o incapacidad total del TRABAJADOR, estos deberán cumplir con los requisitos establecidos en las Disposiciones generales en materia de operaciones de los Sistemas de Ahorro para el Retiro vigentes a la fecha del retiro de los recursos.
"), $iBorder, 'J');
            
			$pdf->Ln($iSaltoLinea2);
			$pdf->SetFont('Tahoma','B',$sizeFontCuerpo2);
			$pdf->setX($SangriaMargen);
			$pdf->Cell( 203, $iAltoRen2, utf8_decode("VIGÉSIMA.- RESPONSABILIDAD DE LA AFORE POR ACTOS DE LAS SOCIEDADES DE INVERSIÓN QUE ADMINISTRE, ASÍ COMO POR LOS ACTOS REALIZADOS POR SUS AGENTES PROMOTORES
"), 0, 'L');
            $pdf->Ln($iSaltoLinea2+$iSaltoLineaExtra);
			
			$pdf->SetFont('Tahoma','',$sizeFontCuerpo2);
			$pdf->setX($SangriaMargen);

			$pdf->MultiCell(($FinMargen+$InicioMargen)-$SangriaMargen,$interliniadoCuerpo, utf8_decode("Conforme a las disposiciones correspondientes, la AFORE responderá directamente de todos los actos, omisiones y operaciones que realicen las SIEFORES que opere, con motivo de su participación en los Sistemas de Ahorro para el Retiro.
Así mismo, la AFORE responderá directamente de los actos realizados por  sus  agentes promotores, ya sea que éstos tengan una relación laboral con la AFORE o sean independientes.
La AFORE no será responsable por actos previos al Traspaso de los recursos de la Cuenta Individual a la misma.
"), $iBorder, 'J');
			$pdf->Ln($iSaltoLinea2);
			$pdf->SetFont('Tahoma','B',$sizeFontCuerpo2);
			$pdf->setX($SangriaMargen);
			$pdf->Cell( 203, $iAltoRen2, utf8_decode('VIGÉSIMA PRIMERA.- CONFIDENCIALIDAD.') , $iBorder/**/, 0, 'L');
			
	        $pdf->Ln($iSaltoLinea2);
			$pdf->SetFont('Tahoma','',$sizeFontCuerpo2);
			$pdf->setX($SangriaMargen);

			$pdf->MultiCell(($FinMargen+$InicioMargen)-$SangriaMargen, $interliniadoCuerpo, utf8_decode("Las partes aceptan expresamente guardar rigurosa confidencialidad respecto a la información que mutuamente se proporcionen para el cumplimiento del presente contrato. Queda estrictamente prohibido para LA AFORE así como  para  quienes formen parte de su personal, mostrar, reproducir, divulgar, retransmitir, utilizar públicamente, distribuir o efectuar cualquier otra forma de uso o explotación sin permiso por escrito de la otra parte, de cualquier  tipo  de  material que le hubiere proporcionado EL TRABAJADOR al amparo de este instrumento, bajo pena de incurrir en alguna sanción establecida por la normatividad aplicable, con excepción de la información que se transmita entre las empresas del grupo Coppel que presten servicios complementarios a la administración de Cuentas Individuales.Las partes darán tratamiento a la información que mutuamente se compartan, con motivo del presente, de acuerdo a lo dispuesto en la ley federal de protección de datos personales en  posesión  de particulares, con la finalidad de garantizar la privacidad de la misma.
En este instrumento LA AFORE acepta que la información que le sea proporcionada por el TRABAJADOR para el cumplimiento del presente contrato y de la que tenga conocimiento durante la  vigencia  del mismo, quedará denominada simplemente como información confidencial, por lo que no requerirá ser marcada como confidencial para que adquiera tal carácter.
La información confidencial no incluye aquella información que sea del dominio público.
"), $iBorder, 'J');

              $rutaPdf = "/sysx/progs/web/salida/solicitudafiliacion/".$iPidActual."SolicitudAfiliacionRegistroH3".$arrDatosSolicitud['curp'].".pdf";
			$respuesta->rutaPdf = "/sysx/progs/web/salida/solicitudafiliacion/".$iPidActual."SolicitudAfiliacionRegistroH3".$arrDatosSolicitud['curp'].".pdf";
			$sRutaAbsoluta = $iPidActual."SolicitudAfiliacionRegistroH3".$arrDatosSolicitud['curp'].".pdf";

			//ARCHIVO TEMPORAL
			$rutaPdfTemp = "/sysx/progs/web/salida/solicitudafiliacion/".$arrDatosSolicitud['folio']."_".$iPidActual."SolicitudAfiliacionRegistroH3".$arrDatosSolicitud['curp']."_TEMP.pdf";

			//RUTA DE LAS FIRMAS
			$sRutaFirmaTrabajador = "/sysx/progs/web/entrada/firmas/UFAF_".$arrDatosSolicitud['folio']."_FTRAB.JPG";
			$sRutaFirmaPromotor = "/sysx/progs/web/entrada/firmas/UFAF_".$arrDatosSolicitud['folio']."_FPROM.JPG";

			try
			{
				/*$sRutaNombreTrabajador = $arrDatosSolicitud['nombre'].' '.$arrDatosSolicitud['paterno'].' '.$arrDatosSolicitud['materno'];
				//$pdf->Image('/sysx/progs/web/entrada/firmas/ST00_'.$arrDatosSolicitud["folio"].'_NTRAB.JPG', 113, 213.5, 70, 12, 'jpg');
				$pdf->SetFont('Arial','B', 7.5);
				$pdf->SetTextColor(0,0,0);
				$pdf->SetXY(112, 149.1);
				$pdf->Write(15, $sRutaNombreTrabajador);*/
				$pdf->Output($rutaPdf);
				$objMetodosExpedienteAfiliacion = new CMetodosExpedienteAfiliacion();


				if ($iFirma == 1)
				{
						$pdf = new FPDI();

						$pageCount = $pdf->setSourceFile('/sysx/progs/web/salida/solicitudafiliacion/'.$sRutaAbsoluta);
						$tplIdx = $pdf->importPage(1);
						$pdf->addPage();
						$pdf->useTemplate($tplIdx, null, null, 0, 0, true);
						$arrResp = $objMetodosExpedienteAfiliacion->ctrlImagenesPorTransmitir(3,$iFolio,$sRutaAbsoluta); //iOpcion 3 [SR00,ST00] Solicitud de Afiliacion
						CLogImpresion::escribirLog("Pasaron los datos del PDF a ctrlImagenesPorTransmitir correctamente.");
						$user_agent = $_SERVER['HTTP_USER_AGENT'];
						$OSname = getPlatform($user_agent);
						if($OSname == "Android"){

							// Envio de correo
							$sDocumento = $sRutaAbsoluta;
							$sUbicacion = "/sysx/progs/web/salida/solicitudafiliacion/";
							$sIpOrigen = $_SERVER["HTTP_HOST"];
							$sTipo = "SR00";

							$arrAPI = array(
								'folio' => $arrDatosSolicitud['folio'],
								'documento' => $sDocumento,
								'ubicacion' => $sUbicacion,
								'iporigen' => $sIpOrigen,
								'tipodocumento' => $sTipo
							);

						}


                    $iContinuar = 1;
					if($iContinuar == 1)
					{
						$pdf->Output($rutaPdf, 'F');
						copy($rutaPdf, $rutaPdfTemp);
						chmod($rutaPdf, 0777);
						
                  
				     $pdf = new FPDI();
				     for ($i = 1; $i <= $pageCount; $i++)
			     	 {
					   $pageCount = $pdf->setSourceFile('/sysx/progs/web/salida/solicitudafiliacion/'.$sRutaAbsoluta);
					   $tplIdx = $pdf->importPage($i);
					   $pdf->addPage();
					   $pdf->useTemplate($tplIdx, null, null, 0, 0, true);
				     }

                        //SE GUARDA PDF
				        $pdf->Output($rutaPdf, 'F');
				        $pdf = new FPDI();
				        //SE AGREGA ESTE "FOR" PARA QUE SE MODIFIQUE SOLAMENTE LA PRIMER PAGINA DEL PDF PERO AL GUARDAR SE GUARDEN TODAS LAS PAGINAS
				        for ($i = 1; $i <= $pageCount; $i++)
			         	{
					      $pageCount = $pdf->setSourceFile('/sysx/progs/web/salida/solicitudafiliacion/'.$sRutaAbsoluta);
					      $tplidx = $pdf->ImportPage($i);
					      $pdf->addPage();
					      $pdf->useTemplate($tplidx, null, null, 0, 0, true);
				        }
    
						//copy($rutaPdf, $rutaPdfTemp);

					     $pdf = new FPDI();
				        for ($i = 1; $i <= $pageCount; $i++)
				        {
				        	$pageCount = $pdf->setSourceFile('/sysx/progs/web/salida/solicitudafiliacion/'.$sRutaAbsoluta);
				        	$tplidx = $pdf->ImportPage($i);
				        	$pdf->addPage();
				        	$pdf->useTemplate($tplIdx, null, null, 0, 0, true);
				        }
				        //SE GUARDA PDF
				
						$pdf->Output($rutaPdf, 'F');

						$user_agent = $_SERVER['HTTP_USER_AGENT'];
						$OSname = getPlatform($user_agent);
						if($OSname == "Android"){

							// create Imagick object
							$imagick = new Imagick();
							// Reads image from PDF
							$imagick->setResolution(500, 500);
							$imagick->readImage($rutaPdf);

							$sRuta_imag = str_replace(".pdf","",$sRutaAbsoluta);

							// Writes an image or image sequence Example- converted-0.jpg, converted-1.jpg
							$imagick->writeImages('/sysx/progs/web/salida/solicitudafiliacion/'.$sRuta_imag.'.tif', false);

						

						}

						$sRutamostrarpdf = "/sysx/progs/web/salida/solicitudafiliacion/".$iPidActual."SolicitudAfiliacionRegistroH3".$arrDatosSolicitud['curp'].".pdf";
						$arrResp['respuesta'] 	= 1;
						$arrResp['mensaje'] 	= 'Exito';
						$arrResp['rutapdf']	    = $sRutamostrarpdf;
						echo json_encode($arrResp);
						CLogImpresion::escribirLog("Se guarda y muestra PDF de REGISTRO");
				
				    }
			     }	
             }
				
			catch (Exception $Ex)
			{
				CLogImpresion::escribirLog("Se borran las imagenes del servidor");
				$respuesta->descestatus = $Ex->getMessage();
				$respuesta->estado = -8;
				CLogImpresion::escribirLog($respuesta->descestatus);
			}

			return($respuesta);
}

function generarSolicitudRegistroHoja4($arrDatosSolicitud, $iFirma, $iFolio)
{
	$iPidActual = getmypid();
	$respuesta = new stdClass();
	$pdf = new PDF_Code39();
	$pdf->AliasNbPages();
	$pdf->AddPage('P','A4');
	$iAltoRen = 5.5;
	$iSaltoLinea = 5.5;
	$sizeFontTitulo = 14;
	$sizeFontCuerpo = 12;
	$sizeFontPiePagina = 6;
	$iRadio = 1;
	$iBordoCel = 0;
	$iBorder = 10;
	$FinMargen = 203;
	$iAltoRen2 = 1;
	$InicioMargen = 4;
	$iAltoRenCabecera = 5.5;
	$iSaltoLineaCabecera = 5;
	$sizeFontCuadro = 11;
	$sizeFontCuerpo2 = 6;
	$SangriaMargen = 5.5;
	$interliniadoCuerpo = 2.6;
	$iSaltoLinea2 = 1;
	$iSaltoLineaExtra=0.3;
	$iSaltoLineaC = 3.5;

	$pdf->SetMargins(4, 4 ,4);

	$pdf->SetFillColor(80,80,80); //Se establece el color de relleno para el encabezado
	$pdf->SetTextColor(255,255,255); //Se establece el color de fuente blanco para el encabezado
	$pdf->SetTextColor(0,0,0);


			$pdf->SetAutoPageBreak(true,15);
			$pdf->Ln($iSaltoLinea2);
			$pdf->SetFont('Tahoma','B',$sizeFontCuerpo2);
			$pdf->setX($SangriaMargen);
			$pdf->Cell( 203, $iAltoRen2, utf8_decode('VIGÉSIMA SEGUNDA.- VIGENCIA DEL CONTRATO') , $iBorder/**/, 0, 'L');
			$pdf->Ln($iSaltoLinea2+1);

			$pdf->SetFont('Tahoma','',$sizeFontCuerpo2);
			$pdf->setX($SangriaMargen);

			$pdf->MultiCell(($FinMargen+$InicioMargen)-$SangriaMargen,$interliniadoCuerpo, utf8_decode("La vigencia del presente contrato será por tiempo indefinido.
Como consecuencia del Registro o Traspaso de la Cuenta Individual del TRABAJADOR en la AFORE, surtirá efectos jurídicos a partir de la inscripción de su solicitud en la Base de Datos Nacional SAR, será hasta ese momento cuando se tenga por manifestado el consentimiento de la AFORE para obligarse en los términos del presente contrato.
"), $iBorder, 'J');



			$pdf->Ln($iSaltoLinea2);
			$pdf->SetFont('Tahoma','B',$sizeFontCuerpo2);
			$pdf->setX($SangriaMargen);
			$pdf->Cell( 203, $iAltoRen2, utf8_decode('VIGÉSIMA TERCERA.- TERMINACIÓN DEL CONTRATO') , $iBorder/**/, 0, 'L');
			$pdf->Ln($iSaltoLinea2+$iSaltoLineaExtra);

			$pdf->SetFont('Tahoma','',$sizeFontCuerpo2);
			$pdf->setX($SangriaMargen);

			$pdf->MultiCell(($FinMargen+$InicioMargen)-$SangriaMargen,$interliniadoCuerpo, utf8_decode("El presente contrato podrá darse por terminado en cualquiera de los siguientes supuestos:
a)Cuando el TRABAJADOR traspase sus recursos a otra Administradora. En este caso el contrato se dará por terminado a partir del momento en que la Empresa Operadora inscriba el Registro en la nueva Administradora elegida;
b)Cuando se cancele la Cuenta Individual del TRABAJADOR por que se ha retirado la totalidad de sus recursos;
c)Cuando la AFORE proceda a su disolución o liquidación. En este caso, se traspasarán las Cuentas Individuales a la nueva Administradora que elija el TRABAJADOR; y
d)En los casos de fallecimiento, presunción de muerte del TRABAJADOR, en  los  supuestos previstos en el Código Civil para el Distrito Federal en materia común y para toda  la  República en materia federal y los códigos civiles de las diversas entidades federativas en que no sea necesaria la previa declaración legal de ausencia o bien en el supuesto de declaración legal de ausencia.
"), $iBorder, 'J');

			$pdf->Ln($iSaltoLinea2);
			$pdf->SetFont('Tahoma','B',$sizeFontCuerpo2);
			$pdf->setX($SangriaMargen);
			$pdf->Cell( 203, $iAltoRen2, utf8_decode('VIGÉSIMA CUARTA.- DOMICILIOS') , $iBorder/**/, 0, 'L');
			$pdf->Ln($iSaltoLinea2+$iSaltoLineaExtra);

			$pdf->SetFont('Tahoma','',$sizeFontCuerpo2);
			$pdf->setX($SangriaMargen);

			$pdf->MultiCell(($FinMargen+$InicioMargen)-$SangriaMargen,$interliniadoCuerpo, utf8_decode("La AFORE señala como su domicilio para servicios y consultas el ubicado en Av Kiki Murillo, Barrio Zona Comercial, Desarrollo Urbano La Primavera, Nivel E2 Local 103-38, C.P. 80199, Culiacán, Sinaloa y el TRABAJADOR el que indica en la Solicitud de Registro o Traspaso en Administradora de Fondos para el Retiro, según sea el caso.
Los avisos y notificaciones que deban darse las partes conforme a este contrato serán enviados a los domicilios anteriores.
Se acuerda expresamente que la AFORE deberá informar al  TRABAJADOR,   a través de los procedimientos establecidos, dentro del mes siguiente a que ocurra cualquier cambio en su  domicilio indicado. El TRABAJADOR por su parte, ratifica la obligación de avisar a  la  AFORE respecto del cambio del domicilio señalado, conforme a lo que se establece en la cláusula cuarta, fracción III, Inciso a) del presente instrumento.
"), $iBorder, 'J');


			$pdf->SetFont('Tahoma','B',$sizeFontCuerpo2);
			$pdf->setX($SangriaMargen);
			$pdf->MultiCell(($FinMargen+$InicioMargen)-$SangriaMargen,$interliniadoCuerpo, utf8_decode("VIGÉSIMA QUINTA.- USO DE EQUIPOS Y SISTEMAS AUTOMATIZADOS O DE TELECOMUNICACIONES PARA LA IDENTIFICACIÓN Y AUTENTICACIÓN DE PERSONAS A TRAVÉS DE MEDIOS ELECTRÓNICOS, ÓPTICOS,  POR  CUALQUIER  OTRA  TECNOLOGÍA O BIEN, POR SIGNOS INEQUÍVOCOS A TRAVÉS DEL USO DE FACTORES DE AUTENTICACIÓN, PARA EL OTORGAMIENTO DE SERVICIOS
"), $iBorder, 'J');

			$pdf->SetFont('Tahoma','',$sizeFontCuerpo2);
			$pdf->setX($SangriaMargen);

			$pdf->MultiCell(($FinMargen+$InicioMargen)-$SangriaMargen,$interliniadoCuerpo, utf8_decode("La AFORE para efectos de la contratación e identificación de la Cuenta Individual y de la prestación de servicios al TRABAJADOR, podrá utilizar medios ópticos y/o electrónicos en los que la huella digital del TRABAJADOR y/o su voz o cualquier otro método de autenticación biométrica autorizado por la CONSAR,servirán como su identificador, pudiendo en su momento sustituir, la firma autógrafa del TRABAJADOR, de conformidad con las Disposiciones generales en materia de operaciones de los Sistemas de Ahorro para el Retiro vigentes.
"), $iBorder, 'J');


			$pdf->SetFont('Tahoma','B',$sizeFontCuerpo2);
			$pdf->setX($SangriaMargen);
			$pdf->MultiCell(($FinMargen+$InicioMargen)-$SangriaMargen,$interliniadoCuerpo, utf8_decode("VIGÉSIMA SEXTA.- SERVICIOS QUE SE BRINDEN AL TRABAJADOR A TRAVÉS DE  MEDIOS ELECTRÓNICOS
"), $iBorder, 'J');

			$pdf->SetFont('Tahoma','',$sizeFontCuerpo2);
			$pdf->setX($SangriaMargen);

			$pdf->MultiCell(($FinMargen+$InicioMargen)-$SangriaMargen,$interliniadoCuerpo, utf8_decode("La AFORE otorgará los medios electrónicos necesarios, con la finalidad de que El TRABAJADOR pueda realizar consultas y/o servicios relacionados a su Cuenta Individual a través de su PÁGINA DE INTERNET www.aforecoppel.com y/o la APP AFORE COPPEL, siempre que El TRABAJADOR cumpla de manera satisfactoria con los requisitos y procesos de autentificación que la AFORE establezca.
"), $iBorder, 'J');

			$pdf->SetFont('Tahoma','B',$sizeFontCuerpo2);
			$pdf->setX($SangriaMargen);
			$pdf->MultiCell(($FinMargen+$InicioMargen)-$SangriaMargen,$interliniadoCuerpo, utf8_decode("VIGÉSIMA SÉPTIMA.- LEGISLACIÓN APLICABLE, TRIBUNALES COMPETENTES Y RECLAMACIONES ANTE LA CONDUSEF
"), $iBorder, 'J');

			$pdf->SetFont('Tahoma','',$sizeFontCuerpo2);
			$pdf->setX($SangriaMargen);
			

			$pdf->MultiCell(($FinMargen+$InicioMargen)-$SangriaMargen,$interliniadoCuerpo, utf8_decode("Para cualquier controversia derivada de la interpretación, cumplimiento  o  ejecución del presente contrato, las partes se someten de manera expresa a las leyes aplicables y tribunales competentes del Distrito Federal o de la ciudad capital de la entidad federativa en que tenga establecido su domicilio el TRABAJADOR, y que se encuentra señalado en la solicitud, renunciando al fuero o jurisdicción que por cualquier causa pudiera corresponderle, sin perjuicio del derecho que la Ley de Protección otorga al TRABAJADOR y sus beneficiarios para presentar sus reclamaciones ante la CONDUSEF.
EL TRABAJADOR o sus beneficiarios podrán presentar reclamación ante la CONDUSEF en sus Oficinas Centrales o en sus Delegaciones Regionales o, en su caso, Estatales o Locales de ubicación más próxima a su domicilio, dentro del término que no exceda de tres meses contados a partir de que se suscite el hecho que motiva la reclamación. En este caso, la AFORE estará obligada a sujetarse únicamente al procedimiento conciliatorio previsto en los artículos 68, 69, 70 y 71 de la Ley de Protección, quedando a libre elección de cada una de las partes el sometimiento o declinación del arbitraje ante la CONDUSEF.
"), $iBorder, 'J');
			

			//FIN 1425.1
		    
	

//FIN 1425.1
            $pdf->Ln($iSaltoLinea * 1.2);
			$pdf->SetFont('Arial','B',$sizeFontCuerpo);
			$pdf->Cell( 208, $iAltoRen, 'BENEFICIARIOS' , $iBorder, 0, 'C');
			$pdf->SetFont('Arial','',$sizeFontCuerpo);
			$pdf->Ln($iSaltoLinea);

			$pdf->RoundedRect(4, $pdf->getY() , 203, $iAltoRen * 11 , $iRadio, 'D', '1234');
			//BENEFICIARIO 1
			$pdf->Cell( 20, $iAltoRen, ' Nombre: ', $iBorder/**/, 0, 'L');
			$pdf->Line($pdf->getX(), $pdf->getY() + $iAltoRen - 0.5 , $pdf->getX() + 181, $pdf->getY() + $iAltoRen - 0.5);
			$pdf->Cell( 186, $iAltoRen, $arrDatosSolicitud['nombres_benf1'].' '.$arrDatosSolicitud['paterno_benf1'].' '.$arrDatosSolicitud['materno_benf1'] , $iBorder/**/, 0, 'L');
			$pdf->Ln($iSaltoLinea);
			$pdf->Cell( 16, $iAltoRen, ' CURP: ' , $iBorder/**/, 0, 'L');
			$pdf->Line($pdf->getX(), $pdf->getY() + $iAltoRen - 0.5 , $pdf->getX() + 74, $pdf->getY() + $iAltoRen - 0.5);
			$pdf->Cell( 74, $iAltoRen, $arrDatosSolicitud['curp_benf1'] , $iBorder/**/, 0, 'L');
			$pdf->Cell( 23, $iAltoRen, 'Porcentaje: ' , $iBorder/**/, 0, 'L');
			$pdf->Line($pdf->getX(), $pdf->getY() + $iAltoRen - 0.5 , $pdf->getX() + 27, $pdf->getY() + $iAltoRen - 0.5);
			if($arrDatosSolicitud['porcentaje_benef1'] != '')
			{
				$pdf->Cell( 27, $iAltoRen, $arrDatosSolicitud['porcentaje_benef1'].sprintf('%% ') , $iBorder/**/, 0, 'L');
			}
			else
			{
				$pdf->Cell( 27, $iAltoRen, '' , $iBorder/**/, 0, 'L');
			}
			$pdf->Cell( 23, $iAltoRen, 'Parentesco: ' , 0/**/, 0, 'L');
			$pdf->Line($pdf->getX(), $pdf->getY() + $iAltoRen - 0.5 , $pdf->getX() + 38, $pdf->getY() + $iAltoRen - 0.5);
			$pdf->Cell( 27, $iAltoRen, $arrDatosSolicitud['parentesco_benef1'] , 0/**/, 0, 'L');
			$pdf->Ln($iSaltoLinea *1.2);

			//BENEFICIARIO 2
			$pdf->Cell( 20, $iAltoRen, ' Nombre: ', 0/**/, 0, 'L');
			$pdf->Line($pdf->getX(), $pdf->getY() + $iAltoRen - 0.5 , $pdf->getX() + 181, $pdf->getY() + $iAltoRen - 0.5);
			$pdf->Cell( 186, $iAltoRen, $arrDatosSolicitud['nombres_benf2'].' '.$arrDatosSolicitud['paterno_benf2'].' '.$arrDatosSolicitud['materno_benf2'] , 0/**/, 0, 'L');
			$pdf->Ln($iSaltoLinea);
			$pdf->Cell( 16, $iAltoRen, ' CURP: ' , 0/**/, 0, 'L');
			$pdf->Line($pdf->getX(), $pdf->getY() + $iAltoRen - 0.5 , $pdf->getX() + 74, $pdf->getY() + $iAltoRen - 0.5);
			$pdf->Cell( 74, $iAltoRen, $arrDatosSolicitud['curp_benf2'] , 0/**/, 0, 'L');
			$pdf->Cell( 23, $iAltoRen, 'Porcentaje: ' , 0/**/, 0, 'L');
			$pdf->Line($pdf->getX(), $pdf->getY() + $iAltoRen - 0.5 , $pdf->getX() + 27, $pdf->getY() + $iAltoRen - 0.5);
			if($arrDatosSolicitud['porcentaje_benef2'] != '')
			{
				$pdf->Cell( 27, $iAltoRen, $arrDatosSolicitud['porcentaje_benef2'].sprintf('%% ') , 0/**/, 0, 'L');
			}
			else
			{
				$pdf->Cell( 27, $iAltoRen, '' , $iBorder/**/, 0, 'L');
			}
			$pdf->Cell( 23, $iAltoRen, 'Parentesco: ' , 0/**/, 0, 'L');
			$pdf->Line($pdf->getX(), $pdf->getY() + $iAltoRen - 0.5 , $pdf->getX() + 38, $pdf->getY() + $iAltoRen - 0.5);
			$pdf->Cell( 27, $iAltoRen, $arrDatosSolicitud['parentesco_benef2'] , 0/**/, 0, 'L');
			$pdf->Ln($iSaltoLinea *1.2);

			//BENEFICIARIO 3
			$pdf->Cell( 20, $iAltoRen, ' Nombre: ', 0/**/, 0, 'L');
			$pdf->Line($pdf->getX(), $pdf->getY() + $iAltoRen - 0.5 , $pdf->getX() + 181, $pdf->getY() + $iAltoRen - 0.5);
			$pdf->Cell( 186, $iAltoRen, $arrDatosSolicitud['nombres_benf3'].' '.$arrDatosSolicitud['paterno_benf3'].' '.$arrDatosSolicitud['materno_benf3'] , 0/**/, 0, 'L');
			$pdf->Ln($iSaltoLinea);
			$pdf->Cell( 16, $iAltoRen, ' CURP: ' , 0/**/, 0, 'L');
			$pdf->Line($pdf->getX(), $pdf->getY() + $iAltoRen - 0.5 , $pdf->getX() + 74, $pdf->getY() + $iAltoRen - 0.5);
			$pdf->Cell( 74, $iAltoRen, $arrDatosSolicitud['curp_benf3'] , 0/**/, 0, 'L');
			$pdf->Cell( 23, $iAltoRen, 'Porcentaje: ' , 0/**/, 0, 'L');
			$pdf->Line($pdf->getX(), $pdf->getY() + $iAltoRen - 0.5 , $pdf->getX() + 27, $pdf->getY() + $iAltoRen - 0.5);
			if($arrDatosSolicitud['porcentaje_benef3'] != '')
			{
				$pdf->Cell( 27, $iAltoRen, $arrDatosSolicitud['porcentaje_benef3'].sprintf('%% ') , 0/**/, 0, 'L');
			}
			else
			{
				$pdf->Cell( 27, $iAltoRen, '' , $iBorder/**/, 0, 'L');
			}
			$pdf->Cell( 23, $iAltoRen, 'Parentesco: ' , 0/**/, 0, 'L');
			$pdf->Line($pdf->getX(), $pdf->getY() + $iAltoRen - 0.5 , $pdf->getX() + 38, $pdf->getY() + $iAltoRen - 0.5);
			$pdf->Cell( 27, $iAltoRen, $arrDatosSolicitud['parentesco_benef3'] , 0/**/, 0, 'L');
			$pdf->Ln($iSaltoLinea *1.2);

			//BENEFICIARIO 4
			$pdf->Cell( 20, $iAltoRen, ' Nombre: ', 0/**/, 0, 'L');
			$pdf->Line($pdf->getX(), $pdf->getY() + $iAltoRen - 0.5 , $pdf->getX() + 181, $pdf->getY() + $iAltoRen - 0.5);
			$pdf->Cell( 186, $iAltoRen, $arrDatosSolicitud['nombres_benf4'].' '.$arrDatosSolicitud['paterno_benf4'].' '.$arrDatosSolicitud['materno_benf4'] , 0/**/, 0, 'L');
			$pdf->Ln($iSaltoLinea);
			$pdf->Cell( 16, $iAltoRen, ' CURP: ' , 0/**/, 0, 'L');
			$pdf->Line($pdf->getX(), $pdf->getY() + $iAltoRen - 0.5 , $pdf->getX() + 74, $pdf->getY() + $iAltoRen - 0.5);
			$pdf->Cell( 74, $iAltoRen, $arrDatosSolicitud['curp_benf4'] , 0/**/, 0, 'L');
			$pdf->Cell( 23, $iAltoRen, 'Porcentaje: ' , 0/**/, 0, 'L');
			$pdf->Line($pdf->getX(), $pdf->getY() + $iAltoRen - 0.5 , $pdf->getX() + 27, $pdf->getY() + $iAltoRen - 0.5);
			if($arrDatosSolicitud['porcentaje_benef4'] != '')
			{
				$pdf->Cell( 27, $iAltoRen, $arrDatosSolicitud['porcentaje_benef4'].sprintf('%% ') , 0/**/, 0, 'L');
			}
			else
			{
				$pdf->Cell( 27, $iAltoRen, '' , $iBorder/**/, 0, 'L');
			}
			$pdf->Cell( 23, $iAltoRen, 'Parentesco: ' , 0/**/, 0, 'L');
			$pdf->Line($pdf->getX(), $pdf->getY() + $iAltoRen - 0.5 , $pdf->getX() + 38, $pdf->getY() + $iAltoRen - 0.5);
			$pdf->Cell( 27, $iAltoRen, $arrDatosSolicitud['parentesco_benef4'] , 0/**/, 0, 'L');
			$pdf->Ln($iSaltoLinea *1.2);

			//BENEFICIARIO 5
			$pdf->Cell( 20, $iAltoRen, ' Nombre: ', 0/**/, 0, 'L');
			$pdf->Line($pdf->getX(), $pdf->getY() + $iAltoRen - 0.5 , $pdf->getX() + 181, $pdf->getY() + $iAltoRen - 0.5);
			$pdf->Cell( 186, $iAltoRen, $arrDatosSolicitud['nombres_benf5'].' '.$arrDatosSolicitud['paterno_benf5'].' '.$arrDatosSolicitud['materno_benf5'] , 0/**/, 0, 'L');
			$pdf->Ln($iSaltoLinea);
			$pdf->Cell( 16, $iAltoRen, ' CURP: ' , 0/**/, 0, 'L');
			$pdf->Line($pdf->getX(), $pdf->getY() + $iAltoRen - 0.5 , $pdf->getX() + 74, $pdf->getY() + $iAltoRen - 0.5);
			$pdf->Cell( 74, $iAltoRen, $arrDatosSolicitud['curp_benf5'] , 0/**/, 0, 'L');
			$pdf->Cell( 23, $iAltoRen, 'Porcentaje: ' , 0/**/, 0, 'L');
			$pdf->Line($pdf->getX(), $pdf->getY() + $iAltoRen - 0.5 , $pdf->getX() + 27, $pdf->getY() + $iAltoRen - 0.5);
			if($arrDatosSolicitud['porcentaje_benef5'] != '')
			{
				$pdf->Cell( 27, $iAltoRen, $arrDatosSolicitud['porcentaje_benef5'].sprintf('%% ') , 0/**/, 0, 'L');
			}
			else
			{
				$pdf->Cell( 27, $iAltoRen, '' , $iBorder/**/, 0, 'L');
			}
			$pdf->Cell( 23, $iAltoRen, 'Parentesco: ' , 0/**/, 0, 'L');
			$pdf->Line($pdf->getX(), $pdf->getY() + $iAltoRen - 0.5 , $pdf->getX() + 38, $pdf->getY() + $iAltoRen - 0.5);
			$pdf->Cell( 27, $iAltoRen, $arrDatosSolicitud['parentesco_benef5'] , 0/**/, 0, 'L');
			$pdf->Ln($iSaltoLinea *1.2);

			//PIE DE PAGINA
			//Posicionar la cabeza de escritura 3 cm de abajo hacia arriba
			$pdf->Ln($iSaltoLinea *1.2);
			//$pdf->SetY(-31);
			$pdf->SetFont('Arial','',$sizeFontPiePagina);
			$pdf->MultiCell( 203, 2.5, utf8_decode("AFORE COPPEL, S.A. de C.V.
				Unidad Especializada de Atención al Público Av. Kiki Murillo No. 103-38 Nivel E2 Módulo C Barrio Zona Comercial, Desarrollo Urbano La Primavera
				Culiacán, Sinaloa, CP. 80199, Teléfonos:(01667)758-91-10 y 01800-26-773-52
				CAT AFORE 01-800-226-7735"), 0, 'C');

           $rutaPdf = "/sysx/progs/web/salida/solicitudafiliacion/".$iPidActual."SolicitudAfiliacionRegistroH4".$arrDatosSolicitud['curp'].".pdf";
			$respuesta->rutaPdf = "/sysx/progs/web/salida/solicitudafiliacion/".$iPidActual."SolicitudAfiliacionRegistroH4".$arrDatosSolicitud['curp'].".pdf";
			$sRutaAbsoluta = $iPidActual."SolicitudAfiliacionRegistroH4".$arrDatosSolicitud['curp'].".pdf";

			//ARCHIVO TEMPORAL
			$rutaPdfTemp = "/sysx/progs/web/salida/solicitudafiliacion/".$arrDatosSolicitud['folio']."_".$iPidActual."SolicitudAfiliacionRegistroH4".$arrDatosSolicitud['curp']."_TEMP.pdf";

			//RUTA DE LAS FIRMAS
			$sRutaFirmaTrabajador = "/sysx/progs/web/entrada/firmas/UFAF_".$arrDatosSolicitud['folio']."_FTRAB.JPG";
			$sRutaFirmaPromotor = "/sysx/progs/web/entrada/firmas/UFAF_".$arrDatosSolicitud['folio']."_FPROM.JPG";

			try
			{
				/*$sRutaNombreTrabajador = $arrDatosSolicitud['nombre'].' '.$arrDatosSolicitud['paterno'].' '.$arrDatosSolicitud['materno'];
				//$pdf->Image('/sysx/progs/web/entrada/firmas/ST00_'.$arrDatosSolicitud["folio"].'_NTRAB.JPG', 113, 213.5, 70, 12, 'jpg');
				$pdf->SetFont('Arial','B', 7.5);
				$pdf->SetTextColor(0,0,0);
				$pdf->SetXY(112, 149.1);
				$pdf->Write(15, $sRutaNombreTrabajador);*/
				$pdf->Output($rutaPdf);
				$objMetodosExpedienteAfiliacion = new CMetodosExpedienteAfiliacion();


				if ($iFirma == 1)
				{
						$pdf = new FPDI();

						$pageCount = $pdf->setSourceFile('/sysx/progs/web/salida/solicitudafiliacion/'.$sRutaAbsoluta);
						$tplIdx = $pdf->importPage(1);
						$pdf->addPage();
						$pdf->useTemplate($tplIdx, null, null, 0, 0, true);
						$arrResp = $objMetodosExpedienteAfiliacion->ctrlImagenesPorTransmitir(3,$iFolio,$sRutaAbsoluta); //iOpcion 3 [SR00,ST00] Solicitud de Afiliacion
						CLogImpresion::escribirLog("Pasaron los 
						PDF a ctrlImagenesPorTransmitir correctamente.");
						$user_agent = $_SERVER['HTTP_USER_AGENT'];
						$OSname = getPlatform($user_agent);
						if($OSname == "Android"){

							// Envio de correo
							$sDocumento = $sRutaAbsoluta;
							$sUbicacion = "/sysx/progs/web/salida/solicitudafiliacion/";
							$sIpOrigen = $_SERVER["HTTP_HOST"];
							$sTipo = "SR00";

							$arrAPI = array(
								'folio' => $arrDatosSolicitud['folio'],
								'documento' => $sDocumento,
								'ubicacion' => $sUbicacion,
								'iporigen' => $sIpOrigen,
								'tipodocumento' => $sTipo
							);

						}


                    $iContinuar = 1;
					if($iContinuar == 1)
					{
						$pdf->Output($rutaPdf, 'F');
						copy($rutaPdf, $rutaPdfTemp);
						chmod($rutaPdf, 0777);
						
                  
				     $pdf = new FPDI();
				     for ($i = 1; $i <= $pageCount; $i++)
			     	 {
					   $pageCount = $pdf->setSourceFile('/sysx/progs/web/salida/solicitudafiliacion/'.$sRutaAbsoluta);
					   $tplIdx = $pdf->importPage($i);
					   $pdf->addPage();
					   $pdf->useTemplate($tplIdx, null, null, 0, 0, true);
				     }

                        //SE GUARDA PDF
				        $pdf->Output($rutaPdf, 'F');
				        $pdf = new FPDI();
				        //SE AGREGA ESTE "FOR" PARA QUE SE MODIFIQUE SOLAMENTE LA PRIMER PAGINA DEL PDF PERO AL GUARDAR SE GUARDEN TODAS LAS PAGINAS
				        for ($i = 1; $i <= $pageCount; $i++)
			         	{
					      $pageCount = $pdf->setSourceFile('/sysx/progs/web/salida/solicitudafiliacion/'.$sRutaAbsoluta);
					      $tplidx = $pdf->ImportPage($i);
					      $pdf->addPage();
					      $pdf->useTemplate($tplidx, null, null, 0, 0, true);
				        }
    
						//copy($rutaPdf, $rutaPdfTemp);

					     $pdf = new FPDI();
				        for ($i = 1; $i <= $pageCount; $i++)
				        {
				        	$pageCount = $pdf->setSourceFile('/sysx/progs/web/salida/solicitudafiliacion/'.$sRutaAbsoluta);
				        	$tplidx = $pdf->ImportPage($i);
				        	$pdf->addPage();
				        	$pdf->useTemplate($tplIdx, null, null, 0, 0, true);
				        }
				        //SE GUARDA PDF
				
						$pdf->Output($rutaPdf, 'F');

						$user_agent = $_SERVER['HTTP_USER_AGENT'];
						$OSname = getPlatform($user_agent);
						if($OSname == "Android"){

							// create Imagick object
							$imagick = new Imagick();
							// Reads image from PDF
							$imagick->setResolution(500, 500);
							$imagick->readImage($rutaPdf);

							$sRuta_imag = str_replace(".pdf","",$sRutaAbsoluta);

							// Writes an image or image sequence Example- converted-0.jpg, converted-1.jpg
							$imagick->writeImages('/sysx/progs/web/salida/solicitudafiliacion/'.$sRuta_imag.'.tif', false);

						

						}

						$sRutamostrarpdf = "/sysx/progs/web/salida/solicitudafiliacion/".$iPidActual."SolicitudAfiliacionRegistroH4".$arrDatosSolicitud['curp'].".pdf";
						$arrResp['respuesta'] 	= 1;
						$arrResp['mensaje'] 	= 'Exito';
						$arrResp['rutapdf']	    = $sRutamostrarpdf;
						echo json_encode($arrResp);
						CLogImpresion::escribirLog("Se guarda y muestra PDF de REGISTRO");
				
				    }
			     }	
             }
				
			catch (Exception $Ex)
			{
				CLogImpresion::escribirLog("Se borran las imagenes del servidor");
				$respuesta->descestatus = $Ex->getMessage();
				$respuesta->estado = -8;
				CLogImpresion::escribirLog($respuesta->descestatus);
			}

			return($respuesta);
}
//Funcion Eduardo
function consultarHuellas($sCurpTrab, $iFolio)
{
	/*Retorno: 0=Sin huellas el prom y trab, 1= existen ambas huellas, 2=Sin hlla prom, 3= sin hlla trab, 4=Error*/
	global $cnxBd;
	global $excepcion;
	global $tiposolicitante;
	$sImgHlla = "";
	$sArchivoWsq = "";
	$sArchivoRaw = "";
	$sArchivoJpg = "";
	$sArchivoTmp = "";
	$sComando = "";
	$sAncho = "";
	$sAlto = "";
	$iRet = 1;
	$bProm = false;
	$bTrab = false;
	$iIDServicio = 9910;
	$user_agent = $_SERVER['HTTP_USER_AGENT'];
	$OSname = getPlatform($user_agent);

	CLogImpresion::escribirLog('Solicitante es --> '.$tiposolicitante );
	if($tiposolicitante != 1)
	{
		$cTipoOperacion = '0405';
	}
	else
	{
		$cTipoOperacion = '0101';
		$iIDServicio = 9909;
	}

	$iContador = 0;

	if($tiposolicitante != 1)
	{
		$sSql="SELECT templateafiliacion::TEXT, tipopersona FROM fnobtenerhuellasdoctosafiliacion('".$iFolio."','". $sCurpTrab ."', '".$iIDServicio."'::SMALLINT, '".$cTipoOperacion."')";
		CLogImpresion::escribirLog( $sSql);
	}
	else
	{
		$sSql="SELECT templateafiliacion::TEXT, tipopersona FROM fnobtenerhuellastercerasfiguras('".$iFolio."','". $sCurpTrab ."', '".$iIDServicio."'::SMALLINT, '".$cTipoOperacion."')";
		CLogImpresion::escribirLog( $sSql);
	}

	$resulSet = $cnxBd->query($sSql);

	if($resulSet)
	{

		foreach($resulSet as $reg)
		{
			CLogImpresion::escribirLog( $reg['tipopersona']);

			if($tiposolicitante !=1)
			{
				$sImgHlla=base64_decode($reg['templateafiliacion']);
			}
			else
			{
				if($reg['tipopersona'] == 2)
				{

					$sImgHlla = base64_decode($reg['templateafiliacion']);
				}
				else
				{
					$sImgHlla = str_replace('_','/',$reg['templateafiliacion']);
					$sImgHlla = str_replace('-','+',$sImgHlla);
					$sImgHlla = utf8_decode($sImgHlla);
					$sImgHlla = base64_decode($sImgHlla);
				}
			}

			if($reg['tipopersona']==2)
			{
				$sArchivoWsq=RUTA_SALIDA_HLLA."PROM_".$iFolio.".wsq";
				$sArchivoRaw=RUTA_SALIDA_HLLA."PROM_".$iFolio.".raw";
				$sArchivoJpg=RUTA_SALIDA_HLLA."PROM_".$iFolio.".jpg";
				$sArchivoTmp=RUTA_SALIDA_HLLA."PROM_".$iFolio.".ncm";

				$user_agent = $_SERVER['HTTP_USER_AGENT'];
				$OSname = getPlatform($user_agent);
				/*($OSname == "Android"){
					file_put_contents($sArchivoJpg, $sImgHlla);
					CLogImpresion::escribirLog( "Se creo Imagen Huella Promotor");
				}*/

				$bProm=true;
			}
			else if($reg['tipopersona']==3)
			{
				$sArchivoWsq=RUTA_SALIDA_HLLA."TRAB_".$iFolio.".wsq";
				$sArchivoRaw=RUTA_SALIDA_HLLA."TRAB_".$iFolio.".raw";
				$sArchivoJpg=RUTA_SALIDA_HLLA."TRAB_".$iFolio.".jpg";
				$sArchivoTmp=RUTA_SALIDA_HLLA."TRAB_".$iFolio.".ncm";

				$user_agent = $_SERVER['HTTP_USER_AGENT'];
				$OSname = getPlatform($user_agent);
				/*if($OSname == "Android"){
					file_put_contents($sArchivoJpg, $sImgHlla);
					CLogImpresion::escribirLog( "Se creo Imagen Huella Promotor");
				}*/

				$bTrab=true;
			}


			//if( file_exists($sArchivoWsq) )
			//unlink($sArchivoWsq);
			$user_agent = $_SERVER['HTTP_USER_AGENT'];
			$OSname = getPlatform($user_agent);
			//if($OSname != "Android"){
				$img = fopen($sArchivoWsq, 'w');

				if( $img )
				{
					fwrite($img, $sImgHlla);
					fclose($img);

					//Se lee el archivo WSQ para conocer las dimensiones de la imagen y determinar las dimensiones que tendra el JPG
					//El Ancho siempre vendra en la linea 2
					//El Alto siempre vendra en la linea 3
					$iContador = 0;
					if ($file = fopen($sArchivoWsq, "r"))
					{
						while(!feof($file))
						{
							$line = fgets($file);
							$iContador = $iContador + 1;
							if ($iContador == 2)
							{
								$sAncho = $line;
							}
							else if ($iContador == 3)
							{
								$sAlto = $line;
								break;
							}
						}
						fclose($file);
					}

					//Se extrae de la linea el valor del Ancho y Alto
					$arrAncho = explode(" ", trim($sAncho));
					$arrAlto = explode(" ", trim($sAlto));
					$iAncho = $arrAncho[1];
					$iAlto = $arrAlto[1];
					CLogImpresion::escribirLog("Ancho de la imagen: ".$iAncho);
					CLogImpresion::escribirLog("Alto de la imagen: ".$iAlto);

					$sComando = APP_DWSQ." raw ". $sArchivoWsq ." -raw_out";
					system($sComando);

					unlink($sArchivoWsq);
					unlink($sArchivoTmp);

					$sImgHlla="";
					//Se asignan las dimensiones a la imagen que se generara de la HUELLA (JPG)
					if($iAncho == 400)
						$sImgHlla="P5\n400 500\n255\n".file_get_contents($sArchivoRaw);
					else if ($iAncho == 357)
						$sImgHlla="P5\n357 392\n255\n".file_get_contents($sArchivoRaw);
					else if($iAncho == 800)
						$sImgHlla="P5\n800 750\n255\n".file_get_contents($sArchivoRaw);
					else if($iAncho == 416)
						$sImgHlla="P5\n416 416\n255\n".file_get_contents($sArchivoRaw);
					else
						$sImgHlla="P5\n292 368\n255\n".file_get_contents($sArchivoRaw);



					//$sImgHlla="P5\n292 368\n255\n".file_get_contents($sArchivoRaw);
					//$sImgHlla="P5\n357 392\n255\n".file_get_contents($sArchivoRaw);

					$imagenBlob = new Imagick();

					$imagenBlob->readImageBlob($sImgHlla);
					$imagenBlob->writeImage($sArchivoJpg);

					unlink($sArchivoRaw);

					if($iAncho == 800)
					{
						$im = new Imagick();
						//Se lee la imagen
						$im->readImage($sArchivoJpg);

						$im->cropImage(400, 500, 200, 100);
						$im->adaptiveResizeImage(360, 413);

						//Se guarda la imagen
						$im->writeImage($sArchivoJpg);
					}



				}
				else
					CLogImpresion::escribirLog('[consultarHuellas] Error al abrir archivo: ' . $sArchivoWsq);
			}
		//}
		if($bTrab && $bProm)
			$iRet=1;
		else if(!$bTrab && $bProm)
			$iRet=3;
		else if($bTrab && !$bProm)
			$iRet=2;
		else
			$iRet=0;
	}
	else
	{
		$arrErr = $cnxBd->errorInfo();
		$iRet=4;
		CLogImpresion::escribirLog('[consultarHuellas] Error: ' . $arrErr[0] . '-' . $arrErr[1] . '-' . $arrErr[2]);
	}
	CLogImpresion::escribirLog('[consultarHuellas] Retorno: ' . $iRet);
	return $iRet;
}

function getPlatform($user_agent) {
	$plataformas = array(
	'Windows 10' => 'Windows NT 10.0+',
	'Windows 8.1' => 'Windows NT 6.3+',
	'Windows 8' => 'Windows NT 6.2+',
	'Windows 7' => 'Windows NT 6.1+',
	'Windows Vista' => 'Windows NT 6.0+',
	'Windows XP' => 'Windows NT 5.1+',
	'Windows 2003' => 'Windows NT 5.2+',
	'Windows' => 'Windows otros',
	'iPhone' => 'iPhone',
	'iPad' => 'iPad',
	'Mac OS X' => '(Mac OS X+)|(CFNetwork+)',
	'Mac otros' => 'Macintosh',
	'Android' => 'Android',
	'BlackBerry' => 'BlackBerry',
	'Linux' => 'Linux',
	);
	foreach($plataformas as $plataforma=>$pattern){
	//if (array_exist($pattern, $user_agent))
	if (preg_match('/'.$pattern.'/', $user_agent))
	return $plataforma;
	}
	return 'No detectado';
}

function validartiposolicitanteexcepcion($iFolioSol)
{
	$datos = array();
	global $cnxBd;
	global $excepcion;
	global $tiposolicitante;
	CLogImpresion::escribirLog("Entro");
	try
	{
		$sSql = "SELECT iexcepcion, itiposolicitante from fnobtenertiposolicitanteexcepcion($iFolioSol)";
		CLogImpresion::escribirLog($sSql);
		$resulSet = $cnxBd->query($sSql);
		if($resulSet)
		{
			foreach($resulSet as $reg2)
			{
				$datos['iexcepcion'] 		= $reg2["iexcepcion"];
				$datos['itiposolicitante'] 	= $reg2["itiposolicitante"];
			}

			CLogImpresion::escribirLog("El iexcepcion es: ".$datos['iexcepcion']);
			CLogImpresion::escribirLog("El itiposolicitante es: ".$datos['itiposolicitante']);
			$excepcion = $datos['iexcepcion'];
			$tiposolicitante = $datos['itiposolicitante'];
		}
		else
		{
			CLogImpresion::escribirLog("Error al obetener el tiposolicitante:".$datos);
		}
	}
	catch (Exception $e) {
			$mensaje= 'Excepcion: ' . $e->getMessage() . ' Linea: ' . $e->getLine() .    '  Codigo: ' .  $e->getCode().'SQL->'.$cSql;
			CLogImpresion::escribirLog("Error en la ejecucion :".$mensaje);
	}

	return $datos;
}

function caracteresEspeciales($text){
	$svar = trim($text);
	$svar = str_replace("#","ñ",$svar);
    $svar = str_replace("Ãx81;x81;","Á",$svar);
    $svar = str_replace("Ã¡","á",$svar);
    $svar = str_replace("Ã‰","É",$svar);
    $svar = str_replace("Ã©","é",$svar);
    $svar = str_replace("Ãx8d;x8d;","Í",$svar);
    $svar = str_replace("Ã­","í",$svar);
    $svar = str_replace("Ã“","Ó",$svar);
    $svar = str_replace("Ã³","ó",$svar);
    $svar = str_replace("Ãš","Ú",$svar);
    $svar = str_replace("Ãº","ú",$svar);
    $svar = str_replace("Ã‘","Ñ",$svar);
    $svar = str_replace("Ã±","ñ",$svar);
    $svar = str_replace("Ãx91;x91;","Ñ",$svar);
	$svar = str_replace("Â¿","¿",$svar);

	$svar = utf8_decode($svar);

	return $svar;
}

?>